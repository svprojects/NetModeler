﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NetModeler
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                try
                {
                    Application.Run(new Form1(args[0]));
                }
                catch (Exception)
                {
                    Application.Run(new Form1());
                }
        }
    }
}
