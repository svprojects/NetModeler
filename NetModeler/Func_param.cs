﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetModeler
{
    public partial class Func_param : Form
    {
        public Func_param(host h, propertis_host ph_t)
        {
            Host = h;
            ph = ph_t;
            InitializeComponent();
            numericUpDown1.Value = Host.a1;
            numericUpDown2.Value = Host.a2;
            string name_func = ph.comboBox_func.SelectedText;
            switch (name_func)
            {
                case "Стандарт":
                    label_a.Text = "α = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible = false;
                    break;
                case "Бета":
                    label_a.Text = "α = ";
                    label_b.Text = "β = ";
                    break;
                case "Бета-премьер":
                    label_a.Text = "α = ";
                    label_b.Text = "β = ";
                    break;
                case "Коши":
                    label_a.Text = "α = ";
                    label_b.Text = "γ = ";
                    break;
                case "Хи-кадрат":
                    label_a.Text = "α = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible=false;
                    break;
                case "Равномерное":
                    label_a.Text = "α = ";
                    label_b.Text = "β = ";
                    break;
                case "Эрланга":
                    label_a.Text = "α = ";
                    label_b.Text = "γ = ";
                    break;
                case "Экспоненциальное":
                    label_a.Text = "λ = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible=false;
                    break;
                case "Фишера-Снедекора":
                    label_a.Text = "α = ";
                    label_b.Text = "β = ";
                    break;
                case "Гамма":
                    label_a.Text = "α = ";
                    label_b.Text = "θ = ";
                    break;
                case "Лапласа":
                    label_a.Text = "α = ";
                    label_b.Text = "μ = ";
                    break;
                case "Логонормальное":
                    label_a.Text = "μ = ";
                    label_b.Text = "σ = ";
                    break;
                case "Нормальное":
                    label_a.Text = "μ = ";
                    label_b.Text = "σ = ";
                    break;
                case "Парето":
                    label_a.Text = "α = ";
                    label_b.Text = "β = ";
                    break;
                case "Рэлея":
                    label_a.Text = "σ = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible = false;
                    break;
                case "Стьюдента":
                    label_a.Text = "ν = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible = false;
                    break;
                case "Вейбулла":
                    label_a.Text = "α = ";
                    label_b.Text = "λ = ";
                    break;
                default:
                    label_a.Text = "α = ";
                    label_b.Visible = false;
                    numericUpDown2.Visible = false;
                    break;
            }
        }
        //события
        private void button2_Click(object sender, EventArgs e)
        {
            Host.a1 = Convert.ToInt32(numericUpDown1.Value);
            Host.a2 = Convert.ToInt32(numericUpDown2.Value);
            this.Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //переменные
        host Host;
        propertis_host ph;


    }
}
