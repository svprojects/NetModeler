﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetModeler
{
    public partial class properties_server : Form
    {
        public properties_server(Form1 tt, server S)
        {
            t = tt;
            Serv = S;
            InitializeComponent();
            Initialization();
        }
        public void Initialization()
        {
            label_id.Text = "id: " + Serv.id.ToString();
            textBox_name.Text = Serv.name;
            textBox_delay_for_admission.Text = Serv.delay_for_admission.ToString();
            textBox_speed_do.Text = Serv.speed_do.ToString();
            numeric_P_virus_start.Value = Convert.ToDecimal(Serv.secure_param.P_virus_start);
            numeric_P_secure_start.Value = Convert.ToDecimal(Serv.secure_param.P_secure_start);
            checkBox_Firewall.Checked = Serv.secure_param.Key_Firewall;
            checkBox_Antivirus.Checked = Serv.secure_param.Key_Antivirus;
            checkBox_ids.Checked = Serv.secure_param.Key_IDS;
            checkBox_Administration.Checked = Serv.secure_param.Key_Administration;
            label_K_secure.Text = String.Format("{0:0.00}", Serv.secure_param.K_secure);
        }
        private object test_dfa(string text)
        {
            int dfa = Convert.ToInt32(text);
            if ((dfa < 0) || (dfa > 50))
                return (new Exception());
            else return dfa;
        }
        private object test_sd(string text)
        {
            int sd = Convert.ToInt32(text);
            if ((sd < 0) || (sd > Int32.MaxValue))
                return (new Exception());
            else return sd;
        }
        //переменные
        Form1 t;
        server Serv;
        //события
        private void checkBox_Firewall_CheckedChanged(object sender, EventArgs e)
        {
            Serv.secure_param.Key_Firewall = checkBox_Firewall.Checked;
            Serv.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Serv.secure_param.K_secure);
        }
        private void checkBox_Antivirus_CheckedChanged(object sender, EventArgs e)
        {
            Serv.secure_param.Key_Antivirus = checkBox_Antivirus.Checked;
            Serv.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Serv.secure_param.K_secure);
        }
        private void checkBox_ids_CheckedChanged(object sender, EventArgs e)
        {
            Serv.secure_param.Key_IDS = checkBox_ids.Checked;
            Serv.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Serv.secure_param.K_secure);
        }
        private void checkBox_Administration_CheckedChanged(object sender, EventArgs e)
        {
            Serv.secure_param.Key_Administration = checkBox_Administration.Checked;
            Serv.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Serv.secure_param.K_secure);
        }
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                Serv.name = textBox_name.Text;
                Serv.delay_for_admission = (int)test_dfa(textBox_delay_for_admission.Text);
                Serv.speed_do = (int)test_sd(textBox_speed_do.Text);

                if (t.Servers.ContainsKey(Serv.id))
                {
                    t.Servers.Remove(Serv.id);
                    t.Servers.Add(Serv.id, Serv);
                    t.elements.Remove(Serv.id);
                    t.elements.Add(Serv.id, Serv);
                }
                else
                {
                    t.Servers.Add(Serv.id, Serv);
                    t.elements.Add(Serv.id, Serv);
                }
                Serv.secure_param.Key_Firewall = checkBox_Firewall.Checked;
                Serv.secure_param.Key_Antivirus = checkBox_Antivirus.Checked;
                Serv.secure_param.Key_IDS = checkBox_ids.Checked;
                Serv.secure_param.Key_Administration = checkBox_Administration.Checked;
                Serv.secure_param.P_virus_start = Convert.ToDouble(numeric_P_virus_start.Value);
                Serv.secure_param.P_secure_start = Convert.ToDouble(numeric_P_secure_start.Value);
                Serv.secure_param.K_secure_set();
                Serv.secure_param.P_apriory_set();
                Serv.secure_param.P_return_secure_apriory_set();
                Serv.label_name.Text = Serv.name;
                t.MainContainer.Panel2.Controls.Add(Serv.panel);
                t.MainContainer.Panel2.Controls.SetChildIndex(Serv.panel, 0);
                this.Close();
                t.Number_of_elem.Text = "Всего элементов:  " + t.elements.Count.ToString();
                t.Enabled = true;
                t.Select();
            }
            catch(Exception)
            {
                MessageBox.Show("Проверьте правильность введенных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            t.Enabled = true;
            t.Select();
        }
        private void properties_server_FormClosed(object sender, FormClosedEventArgs e)
        {
            t.Enabled = true;
            t.Select();
        }
    }
}
