﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic.PowerPacks;

namespace NetModeler
{
    public partial class properties_link : Form
    {
        public properties_link(Form1 tt, link L)
        {
            t = tt;
            Link = L;
            InitializeComponent();
            Initialization();
        }
        public void Initialization()
        {
            label_id.Text = "id: " + Link.id.ToString();
            /*textBox_adress_from.Text = Link.adress_from.ToString();
            textBox_adress_to.Text = Link.adress_to.ToString();*/
            List<string> list_adress_from = new List<string>();
            List<string> list_adress_to = new List<string>();
            foreach (element e in t.elements.Values)
            {
                list_adress_from.Add(e.name + " ( id: " + e.id.ToString() + " )");
                list_adress_to.Add(e.name + " ( id: " + e.id.ToString() + " )");
            }
            comboBox_adress_from.DataSource = list_adress_from;
            comboBox_adress_to.DataSource = list_adress_to;
            try
            {
                comboBox_adress_from.Text = t.elements[Link.adress_from].name + " ( id: " + Link.adress_from.ToString() + " )";
                comboBox_adress_to.Text = t.elements[Link.adress_to].name + " ( id: " + Link.adress_to.ToString() + " )";
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка: невозможно добавить линию связи!","NetModeler ",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            textBox_speed.Text = Link.speed.ToString();
        }
        private void Is_same_link()
        {
            int ad = -1;
            foreach (link c in t.Links.Values)
            {
                if ((c.adress_from == Link.adress_from) && (c.adress_to == Link.adress_to))
                {
                    ad = c.id;
                }
                if ((c.adress_from == Link.adress_to) && (c.adress_to == Link.adress_from))
                {
                    ad = c.id;
                }
            }
            if (t.Links.ContainsKey(ad))
            {
                t.Links.Remove(ad);
            }
        }
        private void Is_adress_in_elements()
        {
            if (!(t.elements.ContainsKey(Link.adress_from)) || (!t.elements.ContainsKey(Link.adress_to)))
                is_link_be = true;
        }
        private int Select_adress(ComboBox box)
        {
            string adress = box.Text;
            try
            {
                int index_1 = adress.LastIndexOf("(");
                int index_2 = adress.LastIndexOf(")");
                string id = adress.Substring(index_1 + 6, index_2 - index_1 - 7);
                return Convert.ToInt32(id);
            }
            catch (Exception)
            {
                return -1;
            }
            //MessageBox.Show(Host.adress_to.ToString());
        }
        private bool test(int speed)
        {
            if ((speed <= 0) || (speed > int.MaxValue)) return false;
            else return true;
        }//проверка введенных данных (скорость)
        //переменные
        Form1 t;
        link Link;
        bool is_link_be = false;
        //события
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            t.Enabled = true;
            t.Select();
        }
        private void button_Save_Click(object sender, EventArgs e)
        {
            is_link_be = false;
            Link.adress_from = Select_adress(comboBox_adress_from);
            Link.adress_to = Select_adress(comboBox_adress_to);
            int speed = 0;
            try
            {
                speed = Convert.ToInt32(textBox_speed.Text);
            }
            catch (Exception)
            {
                is_link_be = true;
                speed = Link.speed;
            }
            if (test(speed))
                Link.speed = speed;
            else
                is_link_be = true;
            Is_same_link();
            Is_adress_in_elements();
            if ((Link.adress_from != Link.adress_to) && (!is_link_be))
            {
                if (t.Links.ContainsKey(Link.id))
                {
                    t.Links.Remove(Link.id);
                    t.Links.Add(Link.id, Link);
                }
                else
                {
                    t.Links.Add(Link.id, Link);
                }
                Link.point_from = t.elements[Link.adress_from].panel.Location;
                Link.point_to = t.elements[Link.adress_to].panel.Location;
                Link.line.StartPoint = Link.point_from;
                Link.line.EndPoint = Link.point_to;
                t.Enabled = true;
                t.Select();
                this.Close();
                
            }
            else MessageBox.Show("Проверьте правильность введенных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void properties_link_FormClosed(object sender, FormClosedEventArgs e)
        {
            t.Enabled = true;
            t.Select();
        }
    }
}
