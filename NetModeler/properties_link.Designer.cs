﻿namespace NetModeler
{
    partial class properties_link
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.label_id = new System.Windows.Forms.Label();
            this.label_adress_from = new System.Windows.Forms.Label();
            this.label_adress_to = new System.Windows.Forms.Label();
            this.label_speed = new System.Windows.Forms.Label();
            this.textBox_speed = new System.Windows.Forms.TextBox();
            this.comboBox_adress_from = new System.Windows.Forms.ComboBox();
            this.comboBox_adress_to = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(335, 201);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 0;
            this.button_Cancel.Text = "Отмена";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(254, 201);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 1;
            this.button_Save.Text = "Сохранить";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(332, 9);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(21, 13);
            this.label_id.TabIndex = 2;
            this.label_id.Text = "id: ";
            // 
            // label_adress_from
            // 
            this.label_adress_from.AutoSize = true;
            this.label_adress_from.Location = new System.Drawing.Point(12, 39);
            this.label_adress_from.Name = "label_adress_from";
            this.label_adress_from.Size = new System.Drawing.Size(79, 13);
            this.label_adress_from.TabIndex = 3;
            this.label_adress_from.Text = "Из элемента: ";
            // 
            // label_adress_to
            // 
            this.label_adress_to.AutoSize = true;
            this.label_adress_to.Location = new System.Drawing.Point(12, 77);
            this.label_adress_to.Name = "label_adress_to";
            this.label_adress_to.Size = new System.Drawing.Size(66, 13);
            this.label_adress_to.TabIndex = 4;
            this.label_adress_to.Text = "В элемент: ";
            // 
            // label_speed
            // 
            this.label_speed.AutoSize = true;
            this.label_speed.Location = new System.Drawing.Point(12, 121);
            this.label_speed.Name = "label_speed";
            this.label_speed.Size = new System.Drawing.Size(108, 13);
            this.label_speed.TabIndex = 5;
            this.label_speed.Text = "Скорость передачи:";
            // 
            // textBox_speed
            // 
            this.textBox_speed.Location = new System.Drawing.Point(126, 118);
            this.textBox_speed.Name = "textBox_speed";
            this.textBox_speed.Size = new System.Drawing.Size(203, 20);
            this.textBox_speed.TabIndex = 8;
            // 
            // comboBox_adress_from
            // 
            this.comboBox_adress_from.FormattingEnabled = true;
            this.comboBox_adress_from.Location = new System.Drawing.Point(126, 36);
            this.comboBox_adress_from.Name = "comboBox_adress_from";
            this.comboBox_adress_from.Size = new System.Drawing.Size(203, 21);
            this.comboBox_adress_from.TabIndex = 9;
            // 
            // comboBox_adress_to
            // 
            this.comboBox_adress_to.FormattingEnabled = true;
            this.comboBox_adress_to.Location = new System.Drawing.Point(126, 74);
            this.comboBox_adress_to.Name = "comboBox_adress_to";
            this.comboBox_adress_to.Size = new System.Drawing.Size(203, 21);
            this.comboBox_adress_to.TabIndex = 10;
            // 
            // properties_link
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 236);
            this.Controls.Add(this.comboBox_adress_to);
            this.Controls.Add(this.comboBox_adress_from);
            this.Controls.Add(this.textBox_speed);
            this.Controls.Add(this.label_speed);
            this.Controls.Add(this.label_adress_to);
            this.Controls.Add(this.label_adress_from);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.button_Cancel);
            this.Name = "properties_link";
            this.Text = "Свойства линии связи";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.properties_link_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_adress_from;
        private System.Windows.Forms.Label label_adress_to;
        private System.Windows.Forms.Label label_speed;
        private System.Windows.Forms.TextBox textBox_speed;
        private System.Windows.Forms.ComboBox comboBox_adress_from;
        private System.Windows.Forms.ComboBox comboBox_adress_to;



    }
}