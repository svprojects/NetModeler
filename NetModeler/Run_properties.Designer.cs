﻿namespace NetModeler
{
    partial class Run_properties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.label_num_of_object = new System.Windows.Forms.Label();
            this.label_timer = new System.Windows.Forms.Label();
            this.textBox_timer = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox_sec = new System.Windows.Forms.TextBox();
            this.label_sec = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button_report = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(525, 326);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 0;
            this.button_Cancel.Text = "Отмена";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(444, 326);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 23);
            this.button_start.TabIndex = 1;
            this.button_start.Text = "Старт";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // label_num_of_object
            // 
            this.label_num_of_object.AutoSize = true;
            this.label_num_of_object.Location = new System.Drawing.Point(13, 13);
            this.label_num_of_object.Name = "label_num_of_object";
            this.label_num_of_object.Size = new System.Drawing.Size(123, 13);
            this.label_num_of_object.TabIndex = 2;
            this.label_num_of_object.Text = "Количество объектов: ";
            // 
            // label_timer
            // 
            this.label_timer.AutoSize = true;
            this.label_timer.Location = new System.Drawing.Point(13, 42);
            this.label_timer.Name = "label_timer";
            this.label_timer.Size = new System.Drawing.Size(99, 13);
            this.label_timer.TabIndex = 3;
            this.label_timer.Text = "Время обработки:";
            // 
            // textBox_timer
            // 
            this.textBox_timer.Location = new System.Drawing.Point(122, 39);
            this.textBox_timer.Name = "textBox_timer";
            this.textBox_timer.Size = new System.Drawing.Size(100, 20);
            this.textBox_timer.TabIndex = 4;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 96);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(414, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // textBox_sec
            // 
            this.textBox_sec.Location = new System.Drawing.Point(122, 70);
            this.textBox_sec.Name = "textBox_sec";
            this.textBox_sec.Size = new System.Drawing.Size(100, 20);
            this.textBox_sec.TabIndex = 7;
            // 
            // label_sec
            // 
            this.label_sec.AutoSize = true;
            this.label_sec.Location = new System.Drawing.Point(13, 73);
            this.label_sec.Name = "label_sec";
            this.label_sec.Size = new System.Drawing.Size(103, 13);
            this.label_sec.TabIndex = 6;
            this.label_sec.Text = "Масштаб времени:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 126);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(414, 183);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // button_report
            // 
            this.button_report.Enabled = false;
            this.button_report.Location = new System.Drawing.Point(363, 326);
            this.button_report.Name = "button_report";
            this.button_report.Size = new System.Drawing.Size(75, 23);
            this.button_report.TabIndex = 9;
            this.button_report.Text = "Отчет";
            this.button_report.UseVisualStyleBackColor = true;
            this.button_report.Click += new System.EventHandler(this.button_report_Click);
            // 
            // Run_properties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 361);
            this.Controls.Add(this.button_report);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox_sec);
            this.Controls.Add(this.label_sec);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.textBox_timer);
            this.Controls.Add(this.label_timer);
            this.Controls.Add(this.label_num_of_object);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button_Cancel);
            this.Name = "Run_properties";
            this.Text = "Построение проекта...";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Run_properties_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Label label_num_of_object;
        private System.Windows.Forms.Label label_timer;
        private System.Windows.Forms.TextBox textBox_timer;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBox_sec;
        private System.Windows.Forms.Label label_sec;
        public System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button_report;
    }
}