﻿namespace NetModeler
{
    partial class properties_server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Save = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.label_id = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label_speed_do = new System.Windows.Forms.Label();
            this.textBox_speed_do = new System.Windows.Forms.TextBox();
            this.label_delay_for_admission = new System.Windows.Forms.Label();
            this.textBox_delay_for_admission = new System.Windows.Forms.TextBox();
            this.label_K_secure = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox_Administration = new System.Windows.Forms.CheckBox();
            this.checkBox_ids = new System.Windows.Forms.CheckBox();
            this.checkBox_Antivirus = new System.Windows.Forms.CheckBox();
            this.checkBox_Firewall = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numeric_P_secure_start = new System.Windows.Forms.NumericUpDown();
            this.numeric_P_virus_start = new System.Windows.Forms.NumericUpDown();
            this.label_P_secure_start = new System.Windows.Forms.Label();
            this.label_P_virus_start = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_secure_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_virus_start)).BeginInit();
            this.SuspendLayout();
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(216, 420);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 0;
            this.Save.Text = "Сохранить";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(297, 420);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 1;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(294, 9);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(18, 13);
            this.label_id.TabIndex = 4;
            this.label_id.Text = "id:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 40);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(65, 13);
            this.label_name.TabIndex = 5;
            this.label_name.Text = "Имя блока:";
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(15, 67);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(297, 20);
            this.textBox_name.TabIndex = 7;
            // 
            // label_speed_do
            // 
            this.label_speed_do.AutoSize = true;
            this.label_speed_do.Location = new System.Drawing.Point(12, 290);
            this.label_speed_do.Name = "label_speed_do";
            this.label_speed_do.Size = new System.Drawing.Size(154, 13);
            this.label_speed_do.TabIndex = 8;
            this.label_speed_do.Text = "Скорость обработки данных:";
            // 
            // textBox_speed_do
            // 
            this.textBox_speed_do.Location = new System.Drawing.Point(15, 317);
            this.textBox_speed_do.Name = "textBox_speed_do";
            this.textBox_speed_do.Size = new System.Drawing.Size(297, 20);
            this.textBox_speed_do.TabIndex = 9;
            // 
            // label_delay_for_admission
            // 
            this.label_delay_for_admission.AutoSize = true;
            this.label_delay_for_admission.Location = new System.Drawing.Point(12, 348);
            this.label_delay_for_admission.Name = "label_delay_for_admission";
            this.label_delay_for_admission.Size = new System.Drawing.Size(135, 13);
            this.label_delay_for_admission.TabIndex = 10;
            this.label_delay_for_admission.Text = "Задержка на прием, сек:";
            // 
            // textBox_delay_for_admission
            // 
            this.textBox_delay_for_admission.Location = new System.Drawing.Point(15, 373);
            this.textBox_delay_for_admission.Name = "textBox_delay_for_admission";
            this.textBox_delay_for_admission.Size = new System.Drawing.Size(297, 20);
            this.textBox_delay_for_admission.TabIndex = 11;
            // 
            // label_K_secure
            // 
            this.label_K_secure.AutoSize = true;
            this.label_K_secure.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_K_secure.Location = new System.Drawing.Point(231, 196);
            this.label_K_secure.Name = "label_K_secure";
            this.label_K_secure.Size = new System.Drawing.Size(83, 39);
            this.label_K_secure.TabIndex = 25;
            this.label_K_secure.Text = "0,00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Коэффициент защиты:";
            // 
            // checkBox_Administration
            // 
            this.checkBox_Administration.AutoSize = true;
            this.checkBox_Administration.Location = new System.Drawing.Point(15, 261);
            this.checkBox_Administration.Name = "checkBox_Administration";
            this.checkBox_Administration.Size = new System.Drawing.Size(173, 17);
            this.checkBox_Administration.TabIndex = 23;
            this.checkBox_Administration.Text = "Разграничение прав доступа";
            this.checkBox_Administration.UseVisualStyleBackColor = true;
            this.checkBox_Administration.CheckedChanged += new System.EventHandler(this.checkBox_Administration_CheckedChanged);
            // 
            // checkBox_ids
            // 
            this.checkBox_ids.AutoSize = true;
            this.checkBox_ids.Location = new System.Drawing.Point(15, 238);
            this.checkBox_ids.Name = "checkBox_ids";
            this.checkBox_ids.Size = new System.Drawing.Size(44, 17);
            this.checkBox_ids.TabIndex = 22;
            this.checkBox_ids.Text = "IDS";
            this.checkBox_ids.UseVisualStyleBackColor = true;
            this.checkBox_ids.CheckedChanged += new System.EventHandler(this.checkBox_ids_CheckedChanged);
            // 
            // checkBox_Antivirus
            // 
            this.checkBox_Antivirus.AutoSize = true;
            this.checkBox_Antivirus.Location = new System.Drawing.Point(15, 215);
            this.checkBox_Antivirus.Name = "checkBox_Antivirus";
            this.checkBox_Antivirus.Size = new System.Drawing.Size(79, 17);
            this.checkBox_Antivirus.TabIndex = 21;
            this.checkBox_Antivirus.Text = "Антивирус";
            this.checkBox_Antivirus.UseVisualStyleBackColor = true;
            this.checkBox_Antivirus.CheckedChanged += new System.EventHandler(this.checkBox_Antivirus_CheckedChanged);
            // 
            // checkBox_Firewall
            // 
            this.checkBox_Firewall.AutoSize = true;
            this.checkBox_Firewall.Location = new System.Drawing.Point(15, 192);
            this.checkBox_Firewall.Name = "checkBox_Firewall";
            this.checkBox_Firewall.Size = new System.Drawing.Size(123, 17);
            this.checkBox_Firewall.TabIndex = 20;
            this.checkBox_Firewall.Text = "Межсетевой экран";
            this.checkBox_Firewall.UseVisualStyleBackColor = true;
            this.checkBox_Firewall.CheckedChanged += new System.EventHandler(this.checkBox_Firewall_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Средства защиты:";
            // 
            // numeric_P_secure_start
            // 
            this.numeric_P_secure_start.DecimalPlaces = 2;
            this.numeric_P_secure_start.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_P_secure_start.Location = new System.Drawing.Point(230, 127);
            this.numeric_P_secure_start.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_P_secure_start.Name = "numeric_P_secure_start";
            this.numeric_P_secure_start.Size = new System.Drawing.Size(85, 20);
            this.numeric_P_secure_start.TabIndex = 29;
            // 
            // numeric_P_virus_start
            // 
            this.numeric_P_virus_start.DecimalPlaces = 2;
            this.numeric_P_virus_start.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_P_virus_start.InterceptArrowKeys = false;
            this.numeric_P_virus_start.Location = new System.Drawing.Point(230, 101);
            this.numeric_P_virus_start.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_P_virus_start.Name = "numeric_P_virus_start";
            this.numeric_P_virus_start.Size = new System.Drawing.Size(85, 20);
            this.numeric_P_virus_start.TabIndex = 28;
            this.numeric_P_virus_start.Value = new decimal(new int[] {
            35,
            0,
            0,
            131072});
            // 
            // label_P_secure_start
            // 
            this.label_P_secure_start.AutoSize = true;
            this.label_P_secure_start.Location = new System.Drawing.Point(12, 131);
            this.label_P_secure_start.Name = "label_P_secure_start";
            this.label_P_secure_start.Size = new System.Drawing.Size(212, 13);
            this.label_P_secure_start.TabIndex = 27;
            this.label_P_secure_start.Text = "Начальная веротность восстановления:";
            // 
            // label_P_virus_start
            // 
            this.label_P_virus_start.AutoSize = true;
            this.label_P_virus_start.Location = new System.Drawing.Point(12, 101);
            this.label_P_virus_start.Name = "label_P_virus_start";
            this.label_P_virus_start.Size = new System.Drawing.Size(191, 13);
            this.label_P_virus_start.TabIndex = 26;
            this.label_P_virus_start.Text = "Начальная вероятность заражения:";
            // 
            // properties_server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 455);
            this.Controls.Add(this.numeric_P_secure_start);
            this.Controls.Add(this.numeric_P_virus_start);
            this.Controls.Add(this.label_P_secure_start);
            this.Controls.Add(this.label_P_virus_start);
            this.Controls.Add(this.label_K_secure);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox_Administration);
            this.Controls.Add(this.checkBox_ids);
            this.Controls.Add(this.checkBox_Antivirus);
            this.Controls.Add(this.checkBox_Firewall);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_delay_for_admission);
            this.Controls.Add(this.label_delay_for_admission);
            this.Controls.Add(this.textBox_speed_do);
            this.Controls.Add(this.label_speed_do);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Save);
            this.Name = "properties_server";
            this.Text = "Свойства сервера";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.properties_server_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_secure_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_virus_start)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label_speed_do;
        private System.Windows.Forms.TextBox textBox_speed_do;
        private System.Windows.Forms.Label label_delay_for_admission;
        private System.Windows.Forms.TextBox textBox_delay_for_admission;
        private System.Windows.Forms.Label label_K_secure;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox_Administration;
        private System.Windows.Forms.CheckBox checkBox_ids;
        private System.Windows.Forms.CheckBox checkBox_Antivirus;
        private System.Windows.Forms.CheckBox checkBox_Firewall;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numeric_P_secure_start;
        private System.Windows.Forms.NumericUpDown numeric_P_virus_start;
        private System.Windows.Forms.Label label_P_secure_start;
        private System.Windows.Forms.Label label_P_virus_start;
    }
}