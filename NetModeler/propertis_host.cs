﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetModeler
{
    public partial class propertis_host : Form
    {
        public propertis_host(Form1 tt, host H)
        {
            t = tt;
            Host = H;
            InitializeComponent();
            Initialization();
        }
        public void Initialization()
        {
            label_id.Text = "id: " + Host.id.ToString();
            textBox_name.Text = Host.name;
            comboBox_func.DataSource = Functions.all_func;
            List<string> list_serevers_do = new List<string>();
            //list_serevers_do.Add("не выбрано ( id: 0 )");
            foreach (server s in t.Servers.Values)
            {
                list_serevers_do.Add(s.name + " ( id: " + s.id.ToString() + " )");
            }
            comboBox_adress.DataSource = list_serevers_do;
            textBox_delay.Text = Host.delay_for_admission.ToString();
            try
            {
                comboBox_adress.Text = t.elements[Host.adress_to].name + " ( id: " + Host.adress_to .ToString() + " )";
            }
            catch (Exception)
            {
                comboBox_adress.Text = "не выбрано";
            }
            //textBox_adress_to.Text = Host.adress_to.ToString();
            numeric_P_virus_start.Value = Convert.ToDecimal(Host.secure_param.P_virus_start);
            numeric_P_secure_start.Value = Convert.ToDecimal(Host.secure_param.P_secure_start);
            checkBox_Firewall.Checked = Host.secure_param.Key_Firewall;
            checkBox_Antivirus.Checked = Host.secure_param.Key_Antivirus;
            checkBox_ids.Checked = Host.secure_param.Key_IDS;
            checkBox_Administration.Checked = Host.secure_param.Key_Administration;
            label_K_secure.Text = String.Format("{0:0.00}", Host.secure_param.K_secure);
            comboBox_func.Text = Init_func(Host.func.Method.Name);
        }
        static public string Init_func(string name_func)
        {
            switch (name_func)
            {
                case "Standart":
                    return "Стандарт";
                case "Beta":
                    return "Бета";
                case "Beta_prime":
                    return "Бета-премьер";
                case "Cauchy":
                    return "Коши";
                case "ChiSquare":
                    return "Хи-кадрат";
                case "ContinuousUniform":
                    return "Равномерное";
                case "Erlang":
                    return "Эрланга";
                case "Exponential":
                    return "Экспоненциальное";
                case "FisherSnedecor":
                    return "Фишера-Снедекора";
                case "Gamma":
                    return "Гамма";
                case "Laplace":
                    return "Лапласа";
                case "Lognormal":
                    return "Логонормальное";
                case "Normal":
                    return "Нормальное";
                case "Pareto":
                    return "Парето";
                case "Rayleigh":
                    return "Рэлея";
                case "StudentsT":
                    return "Стьюдента";
                case "Weibull":
                    return "Вейбулла";
                default:
                    return "Стандарт";
            }
        }
        private object test(string text)
        {
            int dfa = Convert.ToInt32(text);
            if ((dfa < 0) || (dfa > 50))
                return (new Exception());
            else return dfa;
        }
        //переменные
        Form1 t;
        host Host;
        //события
        private void checkBox_Firewall_CheckedChanged(object sender, EventArgs e)
        {
            Host.secure_param.Key_Firewall = checkBox_Firewall.Checked;
            Host.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Host.secure_param.K_secure);
        }
        private void checkBox_Antivirus_CheckedChanged(object sender, EventArgs e)
        {
            Host.secure_param.Key_Antivirus = checkBox_Antivirus.Checked;
            Host.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Host.secure_param.K_secure);
        }
        private void checkBox_ids_CheckedChanged(object sender, EventArgs e)
        {
            Host.secure_param.Key_IDS = checkBox_ids.Checked;
            Host.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Host.secure_param.K_secure);
        }
        private void checkBox_Administration_CheckedChanged(object sender, EventArgs e)
        {
            Host.secure_param.Key_Administration = checkBox_Administration.Checked;
            Host.secure_param.K_secure_set();
            label_K_secure.Text = String.Format("{0:0.00}", Host.secure_param.K_secure);
        }
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                Host.name = textBox_name.Text;
                Host.delay_for_admission = (int)test(textBox_delay.Text);
                Select_function();
                Select_adress_to();
                //Host.adress_to = Convert.ToInt32(textBox_adress_to.Text);
                if (t.Hosts.ContainsKey(Host.id))
                {
                    t.Hosts.Remove(Host.id);
                    t.Hosts.Add(Host.id, Host);
                    t.elements.Remove(Host.id);
                    t.elements.Add(Host.id, Host);
                }
                else
                {
                    t.Hosts.Add(Host.id, Host);
                    t.elements.Add(Host.id, Host);
                }
                Host.secure_param.Key_Firewall = checkBox_Firewall.Checked;
                Host.secure_param.Key_Antivirus = checkBox_Antivirus.Checked;
                Host.secure_param.Key_IDS = checkBox_ids.Checked;
                Host.secure_param.Key_Administration = checkBox_Administration.Checked;
                Host.secure_param.P_virus_start = Convert.ToDouble(numeric_P_virus_start.Value);
                Host.secure_param.P_secure_start = Convert.ToDouble(numeric_P_secure_start.Value);
                Host.secure_param.K_secure_set();
                Host.secure_param.P_apriory_set();
                Host.secure_param.P_return_secure_apriory_set();
                Host.label_name.Text = Host.name;
                t.MainContainer.Panel2.Controls.Add(Host.panel);
                t.MainContainer.Panel2.Controls.SetChildIndex(Host.panel, 0);
                this.Close();
                t.Number_of_elem.Text = "Всего элементов:  " + t.elements.Count.ToString();
                t.Enabled = true;
                t.Select();
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте правильность введенных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            t.Enabled = true;
            t.Select();
        }
        private void propertis_host_FormClosed(object sender, FormClosedEventArgs e)
        {
            t.Enabled = true;
            t.Select();
        }
        void label_params_MouseLeave(object sender, EventArgs e)
        {
            Label l = sender as Label;
            l.Font = new Font("Microsoft Sans Serif", 8 + 25 / 100, FontStyle.Regular);
            l.ForeColor = Color.Black;
        }
        void label_params_MouseMove(object sender, MouseEventArgs e)
        {
            Label l = sender as Label;
            l.Font = new Font("Microsoft Sans Serif", 8 + 25 / 100, FontStyle.Underline);
            l.ForeColor = Color.LightBlue;
        }
        private void label_params_Click(object sender, EventArgs e)
        {
            Func_param param = new Func_param(Host, this);
            param.Show();
        }
        //функции
        private void Select_function()
        {
            string name_func = comboBox_func.Text;
            switch (name_func)
            {
                case "Стандарт":
                    Host.func = Functions.Standart;
                    break;
                case "Бета":
                    Host.func = Functions.Beta;
                    break;
                case "Бета-премьер":
                    Host.func = Functions.Beta_prime;
                    break;
                case "Коши":
                    Host.func = Functions.Cauchy;
                    break;
                case "Хи-кадрат":
                    Host.func = Functions.ChiSquare;
                    break;
                case "Равномерное":
                    Host.func = Functions.ContinuousUniform;
                    break;
                case "Эрланга":
                    Host.func = Functions.Erlang;
                    break;
                case "Экспоненциальное":
                    Host.func = Functions.Exponential;
                    break;
                case "Фишера-Снедекора":
                    Host.func = Functions.FisherSnedecor;
                    break;
                case "Гамма":
                    Host.func = Functions.Gamma;
                    break;
                case "Лапласа":
                    Host.func = Functions.Laplace;
                    break;
                case "Логонормальное":
                    Host.func = Functions.Lognormal;
                    break;
                case "Нормальное":
                    Host.func = Functions.Normal;
                    break;
                case "Парето":
                    Host.func = Functions.Pareto;
                    break;
                case "Рэлея":
                    Host.func = Functions.Rayleigh;
                    break;
                case "Стьюдента":
                    Host.func = Functions.StudentsT;
                    break;
                case "Вейбулла":
                    Host.func = Functions.Weibull;
                    break;
                default:
                    Host.func = Functions.Standart;
                    break;
            }
        }
        private void Select_adress_to() 
        {
            string adress_to = comboBox_adress.Text;
            try
            {
                int index_1 = adress_to.LastIndexOf("(");
                int index_2 = adress_to.LastIndexOf(")");
                string id = adress_to.Substring(index_1 + 6, index_2 - index_1 - 7);
                Host.adress_to = Convert.ToInt32(id);
            }
            catch (Exception)
            {
                Host.adress_to = Host.id;
            }
            //MessageBox.Show(Host.adress_to.ToString());
        }
    }
}
