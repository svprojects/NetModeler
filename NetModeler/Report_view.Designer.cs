﻿namespace NetModeler
{
    partial class Report_view
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.splitContainer_main = new System.Windows.Forms.SplitContainer();
            this.groupBox_server = new System.Windows.Forms.GroupBox();
            this.chart_p = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label3 = new System.Windows.Forms.Label();
            this.label_p_virus = new System.Windows.Forms.Label();
            this.label_k_secure = new System.Windows.Forms.Label();
            this.label_aprs = new System.Windows.Forms.Label();
            this.label_time_virus_all = new System.Windows.Forms.Label();
            this.label_step = new System.Windows.Forms.Label();
            this.chart_serv_qfa = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_serv_queque = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_speed_middle = new System.Windows.Forms.Label();
            this.label_do_all = new System.Windows.Forms.Label();
            this.label_speed_do = new System.Windows.Forms.Label();
            this.label_dfa_serv = new System.Windows.Forms.Label();
            this.label_id_serv = new System.Windows.Forms.Label();
            this.groupBox_host = new System.Windows.Forms.GroupBox();
            this.chart_virus_h = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.label_p_virus_h = new System.Windows.Forms.Label();
            this.label_k_secure_h = new System.Windows.Forms.Label();
            this.label_aprs_h = new System.Windows.Forms.Label();
            this.label_time_virus_all_h = new System.Windows.Forms.Label();
            this.label_step_h = new System.Windows.Forms.Label();
            this.chart_host = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_qft_time = new System.Windows.Forms.Label();
            this.label_adress_to_host = new System.Windows.Forms.Label();
            this.label_func_host = new System.Windows.Forms.Label();
            this.label_dfa_host = new System.Windows.Forms.Label();
            this.label_id_host = new System.Windows.Forms.Label();
            this.label_P_secure_start_serv = new System.Windows.Forms.Label();
            this.label_P_virus_start_serv = new System.Windows.Forms.Label();
            this.label_P_virus_start_host = new System.Windows.Forms.Label();
            this.label_P_secure_start_host = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).BeginInit();
            this.splitContainer_main.Panel2.SuspendLayout();
            this.splitContainer_main.SuspendLayout();
            this.groupBox_server.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_p)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_serv_qfa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_serv_queque)).BeginInit();
            this.groupBox_host.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_virus_h)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_host)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer_main
            // 
            this.splitContainer_main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer_main.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer_main.IsSplitterFixed = true;
            this.splitContainer_main.Location = new System.Drawing.Point(0, 0);
            this.splitContainer_main.Name = "splitContainer_main";
            // 
            // splitContainer_main.Panel2
            // 
            this.splitContainer_main.Panel2.AutoScroll = true;
            this.splitContainer_main.Panel2.AutoScrollMinSize = new System.Drawing.Size(150, 150);
            this.splitContainer_main.Panel2.Controls.Add(this.groupBox_server);
            this.splitContainer_main.Panel2.Controls.Add(this.groupBox_host);
            this.splitContainer_main.Size = new System.Drawing.Size(873, 409);
            this.splitContainer_main.SplitterDistance = 128;
            this.splitContainer_main.SplitterWidth = 1;
            this.splitContainer_main.TabIndex = 0;
            // 
            // groupBox_server
            // 
            this.groupBox_server.AutoSize = true;
            this.groupBox_server.Controls.Add(this.label_P_virus_start_serv);
            this.groupBox_server.Controls.Add(this.label_P_secure_start_serv);
            this.groupBox_server.Controls.Add(this.chart_p);
            this.groupBox_server.Controls.Add(this.label3);
            this.groupBox_server.Controls.Add(this.label_p_virus);
            this.groupBox_server.Controls.Add(this.label_k_secure);
            this.groupBox_server.Controls.Add(this.label_aprs);
            this.groupBox_server.Controls.Add(this.label_time_virus_all);
            this.groupBox_server.Controls.Add(this.label_step);
            this.groupBox_server.Controls.Add(this.chart_serv_qfa);
            this.groupBox_server.Controls.Add(this.chart_serv_queque);
            this.groupBox_server.Controls.Add(this.label2);
            this.groupBox_server.Controls.Add(this.label1);
            this.groupBox_server.Controls.Add(this.label_speed_middle);
            this.groupBox_server.Controls.Add(this.label_do_all);
            this.groupBox_server.Controls.Add(this.label_speed_do);
            this.groupBox_server.Controls.Add(this.label_dfa_serv);
            this.groupBox_server.Controls.Add(this.label_id_serv);
            this.groupBox_server.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_server.Location = new System.Drawing.Point(0, 0);
            this.groupBox_server.Name = "groupBox_server";
            this.groupBox_server.Size = new System.Drawing.Size(751, 1111);
            this.groupBox_server.TabIndex = 1;
            this.groupBox_server.TabStop = false;
            this.groupBox_server.Text = "groupBox_server";
            // 
            // chart_p
            // 
            this.chart_p.BorderlineColor = System.Drawing.Color.Black;
            this.chart_p.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chart_p.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart_p.Legends.Add(legend1);
            this.chart_p.Location = new System.Drawing.Point(31, 867);
            this.chart_p.Name = "chart_p";
            series1.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series1.BackImageTransparentColor = System.Drawing.Color.White;
            series1.BackSecondaryColor = System.Drawing.Color.Transparent;
            series1.BorderColor = System.Drawing.Color.Transparent;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series1.Color = System.Drawing.Color.LightGray;
            series1.EmptyPointStyle.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series1.EmptyPointStyle.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            series1.EmptyPointStyle.BorderColor = System.Drawing.Color.Gray;
            series1.EmptyPointStyle.LabelForeColor = System.Drawing.Color.Transparent;
            series1.EmptyPointStyle.MarkerColor = System.Drawing.Color.Transparent;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Underline);
            series1.LabelBackColor = System.Drawing.Color.White;
            series1.Legend = "Legend1";
            series1.MarkerBorderColor = System.Drawing.Color.Transparent;
            series1.Name = "virus";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.LegendText = "Вероятность восстановления";
            series2.Name = "Восстановление";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.LegendText = "Вероятность заражения";
            series3.Name = "Заражение";
            this.chart_p.Series.Add(series1);
            this.chart_p.Series.Add(series2);
            this.chart_p.Series.Add(series3);
            this.chart_p.Size = new System.Drawing.Size(633, 225);
            this.chart_p.TabIndex = 17;
            this.chart_p.Text = "chart2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 844);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Зависимость вероятностей от времени:";
            // 
            // label_p_virus
            // 
            this.label_p_virus.AutoSize = true;
            this.label_p_virus.Location = new System.Drawing.Point(22, 279);
            this.label_p_virus.Name = "label_p_virus";
            this.label_p_virus.Size = new System.Drawing.Size(191, 13);
            this.label_p_virus.TabIndex = 15;
            this.label_p_virus.Text = "Априорная вероятность заражения:";
            // 
            // label_k_secure
            // 
            this.label_k_secure.AutoSize = true;
            this.label_k_secure.Location = new System.Drawing.Point(22, 234);
            this.label_k_secure.Name = "label_k_secure";
            this.label_k_secure.Size = new System.Drawing.Size(123, 13);
            this.label_k_secure.TabIndex = 14;
            this.label_k_secure.Text = "Коэффициент защиты:";
            // 
            // label_aprs
            // 
            this.label_aprs.AutoSize = true;
            this.label_aprs.Location = new System.Drawing.Point(22, 210);
            this.label_aprs.Name = "label_aprs";
            this.label_aprs.Size = new System.Drawing.Size(261, 13);
            this.label_aprs.TabIndex = 13;
            this.label_aprs.Text = "Априорная вероятность восстановления работы: ";
            // 
            // label_time_virus_all
            // 
            this.label_time_virus_all.AutoSize = true;
            this.label_time_virus_all.Location = new System.Drawing.Point(22, 165);
            this.label_time_virus_all.Name = "label_time_virus_all";
            this.label_time_virus_all.Size = new System.Drawing.Size(124, 13);
            this.label_time_virus_all.TabIndex = 12;
            this.label_time_virus_all.Text = "Общее время простоя:";
            // 
            // label_step
            // 
            this.label_step.AutoSize = true;
            this.label_step.Location = new System.Drawing.Point(22, 142);
            this.label_step.Name = "label_step";
            this.label_step.Size = new System.Drawing.Size(101, 13);
            this.label_step.TabIndex = 11;
            this.label_step.Text = "Степень вершины:";
            // 
            // chart_serv_qfa
            // 
            this.chart_serv_qfa.BorderlineColor = System.Drawing.Color.Black;
            this.chart_serv_qfa.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea1";
            this.chart_serv_qfa.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart_serv_qfa.Legends.Add(legend2);
            this.chart_serv_qfa.Location = new System.Drawing.Point(25, 602);
            this.chart_serv_qfa.Name = "chart_serv_qfa";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.IsVisibleInLegend = false;
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart_serv_qfa.Series.Add(series4);
            this.chart_serv_qfa.Size = new System.Drawing.Size(633, 225);
            this.chart_serv_qfa.TabIndex = 10;
            this.chart_serv_qfa.Text = "chart2";
            // 
            // chart_serv_queque
            // 
            this.chart_serv_queque.BorderlineColor = System.Drawing.Color.Black;
            this.chart_serv_queque.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea3.Name = "ChartArea1";
            this.chart_serv_queque.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart_serv_queque.Legends.Add(legend3);
            this.chart_serv_queque.Location = new System.Drawing.Point(25, 328);
            this.chart_serv_queque.Name = "chart_serv_queque";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.IsVisibleInLegend = false;
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            series6.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series6.BorderColor = System.Drawing.Color.Transparent;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series6.Color = System.Drawing.Color.Silver;
            series6.IsVisibleInLegend = false;
            series6.Legend = "Legend1";
            series6.Name = "virus";
            this.chart_serv_queque.Series.Add(series5);
            this.chart_serv_queque.Series.Add(series6);
            this.chart_serv_queque.Size = new System.Drawing.Size(633, 225);
            this.chart_serv_queque.TabIndex = 9;
            this.chart_serv_queque.Text = "chart1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 579);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(294, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Зависимость размера очереди на передачу от времени:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Зависимость размера буфера данных для обработки от времени:";
            // 
            // label_speed_middle
            // 
            this.label_speed_middle.AutoSize = true;
            this.label_speed_middle.Location = new System.Drawing.Point(22, 120);
            this.label_speed_middle.Name = "label_speed_middle";
            this.label_speed_middle.Size = new System.Drawing.Size(199, 13);
            this.label_speed_middle.TabIndex = 6;
            this.label_speed_middle.Text = "Средняя скорость обработки данных:";
            // 
            // label_do_all
            // 
            this.label_do_all.AutoSize = true;
            this.label_do_all.Location = new System.Drawing.Point(22, 97);
            this.label_do_all.Name = "label_do_all";
            this.label_do_all.Size = new System.Drawing.Size(184, 13);
            this.label_do_all.TabIndex = 5;
            this.label_do_all.Text = "Количество обработанных данных:";
            // 
            // label_speed_do
            // 
            this.label_speed_do.AutoSize = true;
            this.label_speed_do.Location = new System.Drawing.Point(22, 76);
            this.label_speed_do.Name = "label_speed_do";
            this.label_speed_do.Size = new System.Drawing.Size(157, 13);
            this.label_speed_do.TabIndex = 4;
            this.label_speed_do.Text = "Скорость обработки данных: ";
            // 
            // label_dfa_serv
            // 
            this.label_dfa_serv.AutoSize = true;
            this.label_dfa_serv.Location = new System.Drawing.Point(22, 53);
            this.label_dfa_serv.Name = "label_dfa_serv";
            this.label_dfa_serv.Size = new System.Drawing.Size(114, 13);
            this.label_dfa_serv.TabIndex = 3;
            this.label_dfa_serv.Text = "Задержка на прием: ";
            // 
            // label_id_serv
            // 
            this.label_id_serv.AutoSize = true;
            this.label_id_serv.Location = new System.Drawing.Point(22, 32);
            this.label_id_serv.Name = "label_id_serv";
            this.label_id_serv.Size = new System.Drawing.Size(21, 13);
            this.label_id_serv.TabIndex = 2;
            this.label_id_serv.Text = "id: ";
            // 
            // groupBox_host
            // 
            this.groupBox_host.Controls.Add(this.label_P_virus_start_host);
            this.groupBox_host.Controls.Add(this.label_P_secure_start_host);
            this.groupBox_host.Controls.Add(this.chart_virus_h);
            this.groupBox_host.Controls.Add(this.label4);
            this.groupBox_host.Controls.Add(this.label_p_virus_h);
            this.groupBox_host.Controls.Add(this.label_k_secure_h);
            this.groupBox_host.Controls.Add(this.label_aprs_h);
            this.groupBox_host.Controls.Add(this.label_time_virus_all_h);
            this.groupBox_host.Controls.Add(this.label_step_h);
            this.groupBox_host.Controls.Add(this.chart_host);
            this.groupBox_host.Controls.Add(this.label_qft_time);
            this.groupBox_host.Controls.Add(this.label_adress_to_host);
            this.groupBox_host.Controls.Add(this.label_func_host);
            this.groupBox_host.Controls.Add(this.label_dfa_host);
            this.groupBox_host.Controls.Add(this.label_id_host);
            this.groupBox_host.Location = new System.Drawing.Point(9, -128);
            this.groupBox_host.Name = "groupBox_host";
            this.groupBox_host.Size = new System.Drawing.Size(742, 901);
            this.groupBox_host.TabIndex = 0;
            this.groupBox_host.TabStop = false;
            this.groupBox_host.Tag = "";
            this.groupBox_host.Text = "groupBox1";
            // 
            // chart_virus_h
            // 
            this.chart_virus_h.BorderlineColor = System.Drawing.Color.Black;
            this.chart_virus_h.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea4.Name = "ChartArea1";
            this.chart_virus_h.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart_virus_h.Legends.Add(legend4);
            this.chart_virus_h.Location = new System.Drawing.Point(19, 569);
            this.chart_virus_h.Name = "chart_virus_h";
            series7.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series7.BackImageTransparentColor = System.Drawing.Color.White;
            series7.BackSecondaryColor = System.Drawing.Color.Transparent;
            series7.BorderColor = System.Drawing.Color.Transparent;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series7.Color = System.Drawing.Color.LightGray;
            series7.EmptyPointStyle.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series7.EmptyPointStyle.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            series7.EmptyPointStyle.BorderColor = System.Drawing.Color.Gray;
            series7.EmptyPointStyle.LabelForeColor = System.Drawing.Color.Transparent;
            series7.EmptyPointStyle.MarkerColor = System.Drawing.Color.Transparent;
            series7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Underline);
            series7.LabelBackColor = System.Drawing.Color.White;
            series7.Legend = "Legend1";
            series7.MarkerBorderColor = System.Drawing.Color.Transparent;
            series7.Name = "virus";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Legend = "Legend1";
            series8.LegendText = "Вероятность восстановления";
            series8.Name = "Восстановление";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series9.Legend = "Legend1";
            series9.LegendText = "Вероятность заражения";
            series9.Name = "Заражение";
            this.chart_virus_h.Series.Add(series7);
            this.chart_virus_h.Series.Add(series8);
            this.chart_virus_h.Series.Add(series9);
            this.chart_virus_h.Size = new System.Drawing.Size(633, 225);
            this.chart_virus_h.TabIndex = 22;
            this.chart_virus_h.Text = "chart2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 546);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(212, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Зависимость вероятностей от времени:";
            // 
            // label_p_virus_h
            // 
            this.label_p_virus_h.AutoSize = true;
            this.label_p_virus_h.Location = new System.Drawing.Point(15, 252);
            this.label_p_virus_h.Name = "label_p_virus_h";
            this.label_p_virus_h.Size = new System.Drawing.Size(191, 13);
            this.label_p_virus_h.TabIndex = 20;
            this.label_p_virus_h.Text = "Априорная вероятность заражения:";
            // 
            // label_k_secure_h
            // 
            this.label_k_secure_h.AutoSize = true;
            this.label_k_secure_h.Location = new System.Drawing.Point(16, 207);
            this.label_k_secure_h.Name = "label_k_secure_h";
            this.label_k_secure_h.Size = new System.Drawing.Size(123, 13);
            this.label_k_secure_h.TabIndex = 19;
            this.label_k_secure_h.Text = "Коэффициент защиты:";
            // 
            // label_aprs_h
            // 
            this.label_aprs_h.AutoSize = true;
            this.label_aprs_h.Location = new System.Drawing.Point(15, 184);
            this.label_aprs_h.Name = "label_aprs_h";
            this.label_aprs_h.Size = new System.Drawing.Size(261, 13);
            this.label_aprs_h.TabIndex = 18;
            this.label_aprs_h.Text = "Априорная вероятность восстановления работы: ";
            // 
            // label_time_virus_all_h
            // 
            this.label_time_virus_all_h.AutoSize = true;
            this.label_time_virus_all_h.Location = new System.Drawing.Point(15, 138);
            this.label_time_virus_all_h.Name = "label_time_virus_all_h";
            this.label_time_virus_all_h.Size = new System.Drawing.Size(124, 13);
            this.label_time_virus_all_h.TabIndex = 17;
            this.label_time_virus_all_h.Text = "Общее время простоя:";
            // 
            // label_step_h
            // 
            this.label_step_h.AutoSize = true;
            this.label_step_h.Location = new System.Drawing.Point(15, 115);
            this.label_step_h.Name = "label_step_h";
            this.label_step_h.Size = new System.Drawing.Size(101, 13);
            this.label_step_h.TabIndex = 16;
            this.label_step_h.Text = "Степень вершины:";
            // 
            // chart_host
            // 
            this.chart_host.BorderlineColor = System.Drawing.Color.Black;
            this.chart_host.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea5.Name = "ChartArea1";
            this.chart_host.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chart_host.Legends.Add(legend5);
            this.chart_host.Location = new System.Drawing.Point(18, 310);
            this.chart_host.Name = "chart_host";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series10.IsVisibleInLegend = false;
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            series11.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.DarkUpwardDiagonal;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series11.Color = System.Drawing.Color.LightGray;
            series11.IsVisibleInLegend = false;
            series11.Legend = "Legend1";
            series11.Name = "virus";
            this.chart_host.Series.Add(series10);
            this.chart_host.Series.Add(series11);
            this.chart_host.Size = new System.Drawing.Size(584, 224);
            this.chart_host.TabIndex = 5;
            this.chart_host.Text = "chart1";
            // 
            // label_qft_time
            // 
            this.label_qft_time.AutoSize = true;
            this.label_qft_time.Location = new System.Drawing.Point(15, 280);
            this.label_qft_time.Name = "label_qft_time";
            this.label_qft_time.Size = new System.Drawing.Size(296, 13);
            this.label_qft_time.TabIndex = 4;
            this.label_qft_time.Text = "Зависимость загрузки очереди на передачу от времени:";
            // 
            // label_adress_to_host
            // 
            this.label_adress_to_host.AutoSize = true;
            this.label_adress_to_host.Location = new System.Drawing.Point(15, 96);
            this.label_adress_to_host.Name = "label_adress_to_host";
            this.label_adress_to_host.Size = new System.Drawing.Size(205, 13);
            this.label_adress_to_host.TabIndex = 3;
            this.label_adress_to_host.Text = "Адресс сервера обработки траффика: ";
            // 
            // label_func_host
            // 
            this.label_func_host.AutoSize = true;
            this.label_func_host.Location = new System.Drawing.Point(15, 72);
            this.label_func_host.Name = "label_func_host";
            this.label_func_host.Size = new System.Drawing.Size(169, 13);
            this.label_func_host.TabIndex = 2;
            this.label_func_host.Text = "Функция генерации траффика: ";
            // 
            // label_dfa_host
            // 
            this.label_dfa_host.AutoSize = true;
            this.label_dfa_host.Location = new System.Drawing.Point(15, 49);
            this.label_dfa_host.Name = "label_dfa_host";
            this.label_dfa_host.Size = new System.Drawing.Size(114, 13);
            this.label_dfa_host.TabIndex = 1;
            this.label_dfa_host.Text = "Задержка на прием: ";
            // 
            // label_id_host
            // 
            this.label_id_host.AutoSize = true;
            this.label_id_host.Location = new System.Drawing.Point(15, 28);
            this.label_id_host.Name = "label_id_host";
            this.label_id_host.Size = new System.Drawing.Size(21, 13);
            this.label_id_host.TabIndex = 0;
            this.label_id_host.Text = "id: ";
            // 
            // label_P_secure_start_serv
            // 
            this.label_P_secure_start_serv.AutoSize = true;
            this.label_P_secure_start_serv.Location = new System.Drawing.Point(22, 187);
            this.label_P_secure_start_serv.Name = "label_P_secure_start_serv";
            this.label_P_secure_start_serv.Size = new System.Drawing.Size(218, 13);
            this.label_P_secure_start_serv.TabIndex = 18;
            this.label_P_secure_start_serv.Text = "Начальная вероятность восстановления:";
            // 
            // label_P_virus_start_serv
            // 
            this.label_P_virus_start_serv.AutoSize = true;
            this.label_P_virus_start_serv.Location = new System.Drawing.Point(22, 257);
            this.label_P_virus_start_serv.Name = "label_P_virus_start_serv";
            this.label_P_virus_start_serv.Size = new System.Drawing.Size(191, 13);
            this.label_P_virus_start_serv.TabIndex = 19;
            this.label_P_virus_start_serv.Text = "Начальная вероятность заражения:";
            // 
            // label_P_virus_start_host
            // 
            this.label_P_virus_start_host.AutoSize = true;
            this.label_P_virus_start_host.Location = new System.Drawing.Point(16, 230);
            this.label_P_virus_start_host.Name = "label_P_virus_start_host";
            this.label_P_virus_start_host.Size = new System.Drawing.Size(191, 13);
            this.label_P_virus_start_host.TabIndex = 24;
            this.label_P_virus_start_host.Text = "Начальная вероятность заражения:";
            // 
            // label_P_secure_start_host
            // 
            this.label_P_secure_start_host.AutoSize = true;
            this.label_P_secure_start_host.Location = new System.Drawing.Point(16, 160);
            this.label_P_secure_start_host.Name = "label_P_secure_start_host";
            this.label_P_secure_start_host.Size = new System.Drawing.Size(218, 13);
            this.label_P_secure_start_host.TabIndex = 23;
            this.label_P_secure_start_host.Text = "Начальная вероятность восстановления:";
            // 
            // Report_view
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 505);
            this.Controls.Add(this.splitContainer_main);
            this.Name = "Report_view";
            this.Text = "Результаты моделирования";
            this.splitContainer_main.Panel2.ResumeLayout(false);
            this.splitContainer_main.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).EndInit();
            this.splitContainer_main.ResumeLayout(false);
            this.groupBox_server.ResumeLayout(false);
            this.groupBox_server.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_p)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_serv_qfa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_serv_queque)).EndInit();
            this.groupBox_host.ResumeLayout(false);
            this.groupBox_host.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_virus_h)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_host)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer_main;
        private System.Windows.Forms.GroupBox groupBox_host;
        private System.Windows.Forms.Label label_dfa_host;
        private System.Windows.Forms.Label label_id_host;
        private System.Windows.Forms.Label label_qft_time;
        private System.Windows.Forms.Label label_adress_to_host;
        private System.Windows.Forms.Label label_func_host;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_host;
        private System.Windows.Forms.GroupBox groupBox_server;
        private System.Windows.Forms.Label label_dfa_serv;
        private System.Windows.Forms.Label label_id_serv;
        private System.Windows.Forms.Label label_speed_do;
        private System.Windows.Forms.Label label_do_all;
        private System.Windows.Forms.Label label_speed_middle;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_serv_qfa;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_serv_queque;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_time_virus_all;
        private System.Windows.Forms.Label label_step;
        private System.Windows.Forms.Label label_p_virus;
        private System.Windows.Forms.Label label_k_secure;
        private System.Windows.Forms.Label label_aprs;
        private System.Windows.Forms.Label label_p_virus_h;
        private System.Windows.Forms.Label label_k_secure_h;
        private System.Windows.Forms.Label label_aprs_h;
        private System.Windows.Forms.Label label_time_virus_all_h;
        private System.Windows.Forms.Label label_step_h;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_p;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_virus_h;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_P_virus_start_serv;
        private System.Windows.Forms.Label label_P_secure_start_serv;
        private System.Windows.Forms.Label label_P_virus_start_host;
        private System.Windows.Forms.Label label_P_secure_start_host;
    }
}