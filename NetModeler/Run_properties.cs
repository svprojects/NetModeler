﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace NetModeler
{
    public partial class Run_properties : Form
    {
        public Run_properties(Form1 tt)
        {
            t = tt;
            InitializeComponent();
            Initialization();
        }
        private void Initialization()
        {
            label_num_of_object.Text = "Количество объектов: "+t.elements.Count;
            n = t.elements.Count;

        }
        //переменные
        Form1 t;
        int n;//количество потоков
        Dictionary<int, Thread> host_thread = new Dictionary<int, Thread>();
        Dictionary<int, Thread> server_thread = new Dictionary<int, Thread>();
        Dictionary<int, Thread> all_thread = new Dictionary<int, Thread>();
        Dictionary<int, Thread> reports_host_theard = new Dictionary<int, Thread>();
        Dictionary<int, Thread> reports_server_theard = new Dictionary<int, Thread>();
        //функции
        private object test(string text)
        {
            int time = Convert.ToInt32(text);
            if ((time <= 0) || (time > 100))
                return (new Exception());
            else return time;
        }
        //события
        private void Run_properties_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (KeyValuePair<int, Report_host> report_h in t.reports_hosts)
            {
                report_h.Value.quque_for_transfer_time.Clear();
                report_h.Value.p_return_secure_time.Clear();
                report_h.Value.p_virus_time.Clear();
                report_h.Value.virus.Clear();
            }
            foreach (KeyValuePair<int, Report_server> report_s in t.reports_servers)
            {
                report_s.Value.quque_for_transfer_time.Clear();
                report_s.Value.quque_time.Clear();
                report_s.Value.p_return_secure_time.Clear();
                report_s.Value.p_virus_time.Clear();
                report_s.Value.virus.Clear();
            }
            foreach (KeyValuePair<int, element> elem in t.elements)
            {
                elem.Value.secure_param.Clear();
            }
            t.Enabled = true;
            t.Select();
        }
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void button_start_Click(object sender, EventArgs e)
        {
            try
            {
                Run run_element = new Run(this, t);
                element.timer = (int)test(textBox_timer.Text);
                element.sec = (int)test(textBox_sec.Text);
                this.progressBar1.Maximum = element.timer * 1000;
                element.timer = this.progressBar1.Maximum;
                host_thread.Clear();
                server_thread.Clear();
                all_thread.Clear();
                textBox_timer.Enabled = false;
                textBox_sec.Enabled = false;
                foreach (KeyValuePair<int, server> serv in t.Servers)
                {
                    server_thread.Add(serv.Key, new Thread(Run.Run_server));
                    all_thread.Add(serv.Key, new Thread(Run.Run_transfer));
                }
                foreach (KeyValuePair<int, host> host in t.Hosts)
                {
                    host_thread.Add(host.Key, new Thread(Run.Run_host));
                    all_thread.Add(host.Key, new Thread(Run.Run_transfer));
                }
                ///////
                foreach (KeyValuePair<int, Report_host> report_h in t.reports_hosts)
                {
                    reports_host_theard.Add(report_h.Key, new Thread(report_h.Value.Run_report_host));
                }
                foreach (KeyValuePair<int, Report_server> report_s in t.reports_servers)
                {
                    reports_server_theard.Add(report_s.Key, new Thread(report_s.Value.Run_report_server));
                }
                ///////
                foreach (KeyValuePair<int, Thread> host_thread_elem in host_thread)
                    host_thread_elem.Value.Start(t.Hosts[host_thread_elem.Key]);
                foreach (KeyValuePair<int, Thread> server_thread_elem in server_thread)
                    server_thread_elem.Value.Start(t.Servers[server_thread_elem.Key]);
                foreach (KeyValuePair<int, Thread> all_thread_elem in all_thread)
                    all_thread_elem.Value.Start(t.elements[all_thread_elem.Key]);
                ///////
                foreach (KeyValuePair<int, Thread> reports_host_theard_elem in reports_host_theard)
                    reports_host_theard_elem.Value.Start(t.Hosts[reports_host_theard_elem.Key]);
                foreach (KeyValuePair<int, Thread> reports_server_theard_elem in reports_server_theard)
                    reports_server_theard_elem.Value.Start(t.Servers[reports_server_theard_elem.Key]);
                ///////
                Thread newT = new Thread(ProgressBar);
                newT.Start(this);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте правильность введенных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //переменная потока
        public void ProgressBar(object e)
        {
            Run_properties r = e as Run_properties;
            ProgressBar pb = r.progressBar1;
            while (element.timer > 0)
            {
                pb.Invoke(new MethodInvoker(() =>
                    {
                        pb.Value += 10;
                        element.timer -= 10;
                        Thread.Sleep(10);
                    }
                    ));
            }
            MessageBox.Show("Время вышло, работа завершена!");
            foreach (KeyValuePair<int, Thread> host_thread_elem in r.host_thread)
                host_thread_elem.Value.Abort();
            foreach (KeyValuePair<int, Thread> server_thread_elem in r.server_thread)
                server_thread_elem.Value.Abort();
            foreach (KeyValuePair<int, Thread> all_thread_elem in r.all_thread)
                all_thread_elem.Value.Abort();
            pb.Invoke(new MethodInvoker(() =>
            {
                button_report.Enabled = true;
                button_start.Enabled = false;
            }
                    ));
        }
        //обработчики
        private void button_report_Click(object sender, EventArgs e)
        {
            Report_view rep = new Report_view(t);
            rep.Show();
        }
    }
}
