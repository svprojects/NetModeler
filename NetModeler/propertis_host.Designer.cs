﻿namespace NetModeler
{
    partial class propertis_host
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.label_func = new System.Windows.Forms.Label();
            this.label_id = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.label_delay = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.comboBox_func = new System.Windows.Forms.ComboBox();
            this.textBox_delay = new System.Windows.Forms.TextBox();
            this.label_adress_to = new System.Windows.Forms.Label();
            this.label_params = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_Firewall = new System.Windows.Forms.CheckBox();
            this.checkBox_Antivirus = new System.Windows.Forms.CheckBox();
            this.checkBox_ids = new System.Windows.Forms.CheckBox();
            this.checkBox_Administration = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_K_secure = new System.Windows.Forms.Label();
            this.label_P_virus_start = new System.Windows.Forms.Label();
            this.label_P_secure_start = new System.Windows.Forms.Label();
            this.numeric_P_virus_start = new System.Windows.Forms.NumericUpDown();
            this.numeric_P_secure_start = new System.Windows.Forms.NumericUpDown();
            this.comboBox_adress = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_virus_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_secure_start)).BeginInit();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(297, 483);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 0;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(216, 483);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 1;
            this.Save.Text = "Сохранить";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // label_func
            // 
            this.label_func.AutoSize = true;
            this.label_func.Location = new System.Drawing.Point(12, 94);
            this.label_func.Name = "label_func";
            this.label_func.Size = new System.Drawing.Size(156, 13);
            this.label_func.TabIndex = 2;
            this.label_func.Text = "Функция генерации пакетов:";
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(294, 9);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(18, 13);
            this.label_id.TabIndex = 3;
            this.label_id.Text = "id:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 32);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(65, 13);
            this.label_name.TabIndex = 4;
            this.label_name.Text = "Имя блока:";
            // 
            // label_delay
            // 
            this.label_delay.AutoSize = true;
            this.label_delay.Location = new System.Drawing.Point(12, 364);
            this.label_delay.Name = "label_delay";
            this.label_delay.Size = new System.Drawing.Size(125, 13);
            this.label_delay.TabIndex = 5;
            this.label_delay.Text = "Задержка на передачу:";
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(15, 59);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(297, 20);
            this.textBox_name.TabIndex = 6;
            // 
            // comboBox_func
            // 
            this.comboBox_func.FormattingEnabled = true;
            this.comboBox_func.Location = new System.Drawing.Point(15, 121);
            this.comboBox_func.Name = "comboBox_func";
            this.comboBox_func.Size = new System.Drawing.Size(297, 21);
            this.comboBox_func.TabIndex = 7;
            // 
            // textBox_delay
            // 
            this.textBox_delay.Location = new System.Drawing.Point(13, 391);
            this.textBox_delay.Name = "textBox_delay";
            this.textBox_delay.Size = new System.Drawing.Size(299, 20);
            this.textBox_delay.TabIndex = 8;
            // 
            // label_adress_to
            // 
            this.label_adress_to.AutoSize = true;
            this.label_adress_to.Location = new System.Drawing.Point(12, 420);
            this.label_adress_to.Name = "label_adress_to";
            this.label_adress_to.Size = new System.Drawing.Size(142, 13);
            this.label_adress_to.TabIndex = 9;
            this.label_adress_to.Text = "Адрес сервера обработки:";
            // 
            // label_params
            // 
            this.label_params.AutoSize = true;
            this.label_params.Location = new System.Drawing.Point(165, 147);
            this.label_params.Name = "label_params";
            this.label_params.Size = new System.Drawing.Size(147, 13);
            this.label_params.TabIndex = 11;
            this.label_params.Text = "Параметры распределения";
            this.label_params.Click += new System.EventHandler(this.label_params_Click);
            this.label_params.MouseLeave += new System.EventHandler(this.label_params_MouseLeave);
            this.label_params.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label_params_MouseMove);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Средства защиты:";
            // 
            // checkBox_Firewall
            // 
            this.checkBox_Firewall.AutoSize = true;
            this.checkBox_Firewall.Location = new System.Drawing.Point(15, 264);
            this.checkBox_Firewall.Name = "checkBox_Firewall";
            this.checkBox_Firewall.Size = new System.Drawing.Size(123, 17);
            this.checkBox_Firewall.TabIndex = 13;
            this.checkBox_Firewall.Text = "Межсетевой экран";
            this.checkBox_Firewall.UseVisualStyleBackColor = true;
            this.checkBox_Firewall.CheckedChanged += new System.EventHandler(this.checkBox_Firewall_CheckedChanged);
            // 
            // checkBox_Antivirus
            // 
            this.checkBox_Antivirus.AutoSize = true;
            this.checkBox_Antivirus.Location = new System.Drawing.Point(15, 287);
            this.checkBox_Antivirus.Name = "checkBox_Antivirus";
            this.checkBox_Antivirus.Size = new System.Drawing.Size(79, 17);
            this.checkBox_Antivirus.TabIndex = 14;
            this.checkBox_Antivirus.Text = "Антивирус";
            this.checkBox_Antivirus.UseVisualStyleBackColor = true;
            this.checkBox_Antivirus.CheckedChanged += new System.EventHandler(this.checkBox_Antivirus_CheckedChanged);
            // 
            // checkBox_ids
            // 
            this.checkBox_ids.AutoSize = true;
            this.checkBox_ids.Location = new System.Drawing.Point(15, 310);
            this.checkBox_ids.Name = "checkBox_ids";
            this.checkBox_ids.Size = new System.Drawing.Size(44, 17);
            this.checkBox_ids.TabIndex = 15;
            this.checkBox_ids.Text = "IDS";
            this.checkBox_ids.UseVisualStyleBackColor = true;
            this.checkBox_ids.CheckedChanged += new System.EventHandler(this.checkBox_ids_CheckedChanged);
            // 
            // checkBox_Administration
            // 
            this.checkBox_Administration.AutoSize = true;
            this.checkBox_Administration.Location = new System.Drawing.Point(15, 333);
            this.checkBox_Administration.Name = "checkBox_Administration";
            this.checkBox_Administration.Size = new System.Drawing.Size(173, 17);
            this.checkBox_Administration.TabIndex = 16;
            this.checkBox_Administration.Text = "Разграничение прав доступа";
            this.checkBox_Administration.UseVisualStyleBackColor = true;
            this.checkBox_Administration.CheckedChanged += new System.EventHandler(this.checkBox_Administration_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Коэффициент защиты:";
            // 
            // label_K_secure
            // 
            this.label_K_secure.AutoSize = true;
            this.label_K_secure.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_K_secure.Location = new System.Drawing.Point(231, 260);
            this.label_K_secure.Name = "label_K_secure";
            this.label_K_secure.Size = new System.Drawing.Size(83, 39);
            this.label_K_secure.TabIndex = 18;
            this.label_K_secure.Text = "0,00";
            // 
            // label_P_virus_start
            // 
            this.label_P_virus_start.AutoSize = true;
            this.label_P_virus_start.Location = new System.Drawing.Point(12, 173);
            this.label_P_virus_start.Name = "label_P_virus_start";
            this.label_P_virus_start.Size = new System.Drawing.Size(191, 13);
            this.label_P_virus_start.TabIndex = 19;
            this.label_P_virus_start.Text = "Начальная вероятность заражения:";
            // 
            // label_P_secure_start
            // 
            this.label_P_secure_start.AutoSize = true;
            this.label_P_secure_start.Location = new System.Drawing.Point(12, 203);
            this.label_P_secure_start.Name = "label_P_secure_start";
            this.label_P_secure_start.Size = new System.Drawing.Size(212, 13);
            this.label_P_secure_start.TabIndex = 20;
            this.label_P_secure_start.Text = "Начальная веротность восстановления:";
            // 
            // numeric_P_virus_start
            // 
            this.numeric_P_virus_start.DecimalPlaces = 2;
            this.numeric_P_virus_start.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_P_virus_start.InterceptArrowKeys = false;
            this.numeric_P_virus_start.Location = new System.Drawing.Point(230, 173);
            this.numeric_P_virus_start.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_P_virus_start.Name = "numeric_P_virus_start";
            this.numeric_P_virus_start.Size = new System.Drawing.Size(85, 20);
            this.numeric_P_virus_start.TabIndex = 21;
            this.numeric_P_virus_start.Value = new decimal(new int[] {
            35,
            0,
            0,
            131072});
            // 
            // numeric_P_secure_start
            // 
            this.numeric_P_secure_start.DecimalPlaces = 2;
            this.numeric_P_secure_start.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numeric_P_secure_start.Location = new System.Drawing.Point(230, 199);
            this.numeric_P_secure_start.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_P_secure_start.Name = "numeric_P_secure_start";
            this.numeric_P_secure_start.Size = new System.Drawing.Size(85, 20);
            this.numeric_P_secure_start.TabIndex = 22;
            // 
            // comboBox_adress
            // 
            this.comboBox_adress.FormattingEnabled = true;
            this.comboBox_adress.Location = new System.Drawing.Point(15, 445);
            this.comboBox_adress.Name = "comboBox_adress";
            this.comboBox_adress.Size = new System.Drawing.Size(297, 21);
            this.comboBox_adress.TabIndex = 23;
            // 
            // propertis_host
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 518);
            this.Controls.Add(this.comboBox_adress);
            this.Controls.Add(this.numeric_P_secure_start);
            this.Controls.Add(this.numeric_P_virus_start);
            this.Controls.Add(this.label_P_secure_start);
            this.Controls.Add(this.label_P_virus_start);
            this.Controls.Add(this.label_K_secure);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox_Administration);
            this.Controls.Add(this.checkBox_ids);
            this.Controls.Add(this.checkBox_Antivirus);
            this.Controls.Add(this.checkBox_Firewall);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_params);
            this.Controls.Add(this.label_adress_to);
            this.Controls.Add(this.textBox_delay);
            this.Controls.Add(this.comboBox_func);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.label_delay);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.label_func);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Cancel);
            this.Name = "propertis_host";
            this.Text = "Свойства генератора";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.propertis_host_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_virus_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_P_secure_start)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label label_func;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_delay;
        private System.Windows.Forms.TextBox textBox_name;
        public System.Windows.Forms.ComboBox comboBox_func;
        private System.Windows.Forms.TextBox textBox_delay;
        private System.Windows.Forms.Label label_adress_to;
        private System.Windows.Forms.Label label_params;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_Firewall;
        private System.Windows.Forms.CheckBox checkBox_Antivirus;
        private System.Windows.Forms.CheckBox checkBox_ids;
        private System.Windows.Forms.CheckBox checkBox_Administration;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_K_secure;
        private System.Windows.Forms.Label label_P_virus_start;
        private System.Windows.Forms.Label label_P_secure_start;
        private System.Windows.Forms.NumericUpDown numeric_P_virus_start;
        private System.Windows.Forms.NumericUpDown numeric_P_secure_start;
        private System.Windows.Forms.ComboBox comboBox_adress;
    }
}