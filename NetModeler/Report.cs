﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Troschuetz.Random;

namespace NetModeler
{
    public class Report
    {
        public Report(Form1 tt)
        {
            t = tt;
        }
        //переменные
        public Form1 t;
        public int id;
        public string name;
        protected int time_really = 0;
        public Dictionary<int, int> quque_for_transfer_time = new Dictionary<int, int>();//данных в очереди на передачу по времени <вермя, данных>
        public Dictionary<int, double> p_virus_time = new Dictionary<int, double>();//данных в очереди на передачу по времени <вермя, данных>
        public Dictionary<int, double> p_return_secure_time = new Dictionary<int, double>();//данных в очереди на передачу по времени <вермя, данных>
        public Dictionary<int, double> virus = new Dictionary<int, double>();//данных в очереди на передачу по времени <вермя, данных>
    }

    public class Report_server : Report
    {
        public Report_server(Form1 tt)
            : base(tt)
        {
            
        }
        //переменные
        public int do_all = 0;//количество обработанных данных
        public Dictionary<int, int> quque_time = new Dictionary<int, int>();//данных в очереди на обработку по времени <вермя, данных>
        public double speed_middle = 0;//средняя скорость обработки данных
        //функции создания статистики
        public void Run_report_server(object S)
        {
            server serv = S as server;
            string time = " ";
            string dat = " ";
            time_really = element.timer;
            double maxValue = 0;
            List<int> change_list = new List<int>();
            while (element.timer > 0)
            {
                int data_sum = 0;
                //проверяем очередь на передачу
                foreach (int data in serv.quque_for_transfer.Values)
                {
                    data_sum += data;
                }
                if (!quque_for_transfer_time.ContainsKey(time_really - element.timer) && (!quque_time.ContainsKey(time_really - element.timer)))
                {
                    quque_for_transfer_time.Add(time_really - element.timer, data_sum);
                    //проверяем очередь на обработку
                    quque_time.Add(time_really - element.timer, serv.queue);
                    p_virus_time.Add(time_really - element.timer, serv.secure_param.P_dinamic);
                    p_return_secure_time.Add(time_really - element.timer, serv.secure_param.P_return_secure_dinamic);
                    virus.Add(time_really - element.timer, Convert.ToDouble(serv.secure_param.Key_virus));
                    time += " " + (time_really - element.timer).ToString() + " ";
                    dat += " " + data_sum.ToString() + " ";
                    Thread.Sleep(100);
                }
            }
            if(p_virus_time.Values.Max()>=p_return_secure_time.Values.Max()) maxValue = p_virus_time.Values.Max();
            else maxValue = p_return_secure_time.Values.Max();
            foreach (int Key_val in virus.Keys)
            {
                if (virus[Key_val] >= 1) change_list.Add(Key_val);
            }
            foreach (int i in change_list)
            {
                virus[i] = maxValue;
            }
            do_all = serv.data;
            speed_middle = do_all*1000 / (time_really*element.sec);
            //MessageBox.Show("Run_report_server, работа завершена!");
            //MessageBox.Show(do_all + "\nserver\n" + speed_middle + "\ntime_really" + time_really);
        }//работа сервера по обработке траффика
    }

    public class Report_host : Report
    {
        public Report_host(Form1 tt)
            : base(tt)
        {
            
        }
        //функции создания статистики
        public void Run_report_host(object H)
        {
            host host = H as host;
            string time = " ";
            string dat = " ";
            time_really = element.timer;
            List<int> change_list = new List<int>();
            double maxValue = 0;
            while (element.timer > 0)
            {
                int data_sum = 0;
                foreach (int data in host.quque_for_transfer.Values)
                {
                    data_sum += data;
                }
                if (!quque_for_transfer_time.ContainsKey(time_really - element.timer))
                {
                    quque_for_transfer_time.Add(time_really - element.timer, data_sum);
                    p_virus_time.Add(time_really - element.timer,host.secure_param.P_dinamic);
                    p_return_secure_time.Add(time_really - element.timer, host.secure_param.P_return_secure_dinamic);
                    virus.Add(time_really - element.timer, Convert.ToDouble(host.secure_param.Key_virus));
                    time += " " + (time_really - element.timer).ToString() + " ";
                    dat += " " + data_sum.ToString() + " ";
                    Thread.Sleep(100);
                }
            }
            if (p_virus_time.Values.Max() >= p_return_secure_time.Values.Max()) maxValue = p_virus_time.Values.Max();
            else maxValue = p_return_secure_time.Values.Max();
            foreach (int Key_val in virus.Keys)
            {
                if (virus[Key_val] >= 1) change_list.Add(Key_val);
            }
            foreach(int i in change_list)
            {
                virus[i] = maxValue;
            }
            //MessageBox.Show("Run_report_host, работа завершена!");
            //MessageBox.Show(time+"\nhost\n"+dat);
        }//работа хоста по генерации траффика
    }
}
