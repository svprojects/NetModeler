﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Microsoft.VisualBasic.PowerPacks;

namespace NetModeler
{
    public delegate int generation_of_func(int a, int b);//функция генерации информации, возвращает кол-во бит

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Initialization();
            MainContainer.Visible = false;
        }
        public Form1(string file_name)
        {
            try
            {
                InitializeComponent();
                Initialization();
                OpenBinaryFormat(file_name);
                Save_dictionary.FileName = file_name;
                this.Text = "NetModeler 1.0 - " + file_name;
                this.MainContainer.Visible = true;
                this.проектToolStripMenuItem.Enabled = true;
                save_as_ToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;
            }
            catch (Exception)
            {
                InitializeComponent();
                Initialization();
                MainContainer.Visible = false;
                this.проектToolStripMenuItem.Enabled = false;
                save_as_ToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
            }
        }
        public void Initialization()
        {
            //элементы списка
            name_of_element.Add("Генератор");
            name_of_element.Add("Сервер");
            //name_of_element.Add("Задержка");
            name_of_element.Add("Линия связи");
            //добавление элементов на форму left_listbox
            left_listbox.DataSource = name_of_element;
            //заполенение строки состояния
            Number_of_elem.Text += "  0";
        }//дополнительная инициализация
        //переменные
        public static List<string> name_of_element = new List<string>();//хранит элементы списка
        public static object change = new object();//хранит выбранный в панели инструментов объект 
        public Dictionary<int, host> Hosts = new Dictionary<int, host>();
        public Dictionary<int, server> Servers = new Dictionary<int, server>();
        //public Dictionary<int, ping> Pings = new Dictionary<int, ping>();
        public Dictionary<int, link> Links = new Dictionary<int, link>();
        public static Dictionary<int, Dictionary<int, int>> table_of_way = new Dictionary<int, Dictionary<int, int>>();//матрица смежности
        public Dictionary<int, element> elements = new Dictionary<int, element>();
        //перемнные отчетов
        public Dictionary<int, Report> reports = new Dictionary<int, Report>();
        public Dictionary<int, Report_host> reports_hosts = new Dictionary<int, Report_host>();
        public Dictionary<int, Report_server> reports_servers = new Dictionary<int, Report_server>();
        //События
        private void left_listbox_DoubleClick(object sender, EventArgs e)
        {
            switch (left_listbox.SelectedItem.ToString())
            {
                case "Генератор":
                    propertis_host prop_host = new propertis_host(this,new host(this));
                    this.Enabled = false;
                    prop_host.Show();
                    break;
                case "Сервер":
                    properties_server prop_server = new properties_server(this,new server(this));
                    this.Enabled = false;
                    prop_server.Show();
                    break;
                case "Задержка":
                    properties_ping prop_ping = new properties_ping();
              //      this.Enabled = false;
                    prop_ping.Show();
                    break;
                case "Линия связи":
                    properties_link prop_link = new properties_link(this,new link(this));
                    this.Enabled = false;
                    prop_link.Show();
                    break;
                default: break;
            }
        }
        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (elements.Count != 0)
            {
                var result = MessageBox.Show("Сохранять проект?", "NetModeler", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    сохранитьКакToolStripMenuItem_Click(null, new EventArgs());
                    очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
                    this.MainContainer.Visible = false;
                    this.проектToolStripMenuItem.Enabled = false;
                    save_as_ToolStripMenuItem.Enabled = false;
                    saveToolStripMenuItem.Enabled = false;
                    Save_dictionary.FileName = null;
                    //this.Close();
                }
                else
                {
                    if (result == DialogResult.No)
                    {
                        очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
                        this.MainContainer.Visible = false;
                        this.проектToolStripMenuItem.Enabled = false;
                        save_as_ToolStripMenuItem.Enabled = false;
                        saveToolStripMenuItem.Enabled = false;
                        Save_dictionary.FileName = null;
                        //this.Close();
                    }
                    else { }
                }
            }
            else
            {
                очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
                this.MainContainer.Visible = false;
                this.проектToolStripMenuItem.Enabled = false;
                save_as_ToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
                Save_dictionary.FileName = null;
                //this.Close();
            }
        }
        private void очиститьПолеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (element el in elements.Values)
            {
                el.Dispose();
            }
            foreach (link l in Links.Values)
            {
                l.Dispose();
            }
            Hosts.Clear();
            Servers.Clear();
            //Pings.Clear();
            elements.Clear();
            reports.Clear();
            reports_hosts.Clear();
            reports_servers.Clear();
            Links.Clear();
            Number_of_elem.Text = "Всего элементов:  " + elements.Count.ToString();
        }
        private void запуститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (elements.Count == 0)
            {
                MessageBox.Show("Невозможно запустить пустой проект!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int n = elements.Count;// количество полей в матрице смежности
            table_of_way.Clear();
            foreach (element elem in elements.Values)
            {
                table_of_way.Add(elem.id,new Dictionary<int,int>());
            }
            //инициализация матрицы смежности
            foreach(Dictionary<int,int> tow_string in table_of_way.Values)
                foreach (element elem in elements.Values)
                    tow_string.Add(elem.id, 0);
            foreach (link lin in Links.Values)
            {
                table_of_way[lin.adress_from][lin.adress_to] = lin.speed;
                table_of_way[lin.adress_to][lin.adress_from] = lin.speed;
            }
            foreach (KeyValuePair<int,element> elem in elements)
            {
                int num = 0;//количество соседей у элемента
                foreach (int item in table_of_way[elem.Key].Values)
                {
                    if (item != 0)
                        num++;
                }
                elem.Value.secure_param.n = num;
            }
            if (test_bebore_run())
            {
                Run_properties run_prop = new Run_properties(this);
                this.Enabled = false;
                run_prop.Show();
            }
        }
        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (save_as.ShowDialog() == DialogResult.OK)
            {
                string fileName = save_as.FileName;
                SaveAsBinaryFormat(fileName);
            }
        }
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new_project_ToolStripMenuItem_Click(null, new EventArgs());
            switch (open_file.ShowDialog())
            {
                case DialogResult.Cancel:
                    this.MainContainer.Visible = false;
                    this.проектToolStripMenuItem.Enabled = false;
                    save_as_ToolStripMenuItem.Enabled = false;
                    saveToolStripMenuItem.Enabled = false;
                    break;
                case DialogResult.OK:
                    string fileName = open_file.FileName;
                    OpenBinaryFormat(fileName);
                    Save_dictionary.FileName = fileName;
                    this.Text = "NetModeler 1.0 - " + fileName;
                    this.MainContainer.Visible = true;
                    this.проектToolStripMenuItem.Enabled = true;
                    save_as_ToolStripMenuItem.Enabled = true;
                    saveToolStripMenuItem.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void new_project_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (elements.Count != 0)
            {
                var result = MessageBox.Show("Сохранять проект?", "NetModeler", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    сохранитьКакToolStripMenuItem_Click(null, new EventArgs());
                    очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
                }
                else
                {
                    if (result == DialogResult.No)
                    {
                        очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
                    }
                    else { }
                }
            }
            else
            {
                очиститьПолеToolStripMenuItem_Click(null, new EventArgs());
            }
            MainContainer.Visible = true;
            this.проектToolStripMenuItem.Enabled = true;
            save_as_ToolStripMenuItem.Enabled = true;
            saveToolStripMenuItem.Enabled = true;
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAsBinaryFormat(Save_dictionary.FileName);
            }
            catch(ArgumentNullException ex)
            {
                сохранитьКакToolStripMenuItem_Click(null, new EventArgs());
            }
        }
        //функции
        private void SaveAsBinaryFormat(string file_name)
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            Save_dictionary save_dict = new Save_dictionary(this);
            using (Stream fStream = new FileStream(file_name, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, save_dict);
            }
            MessageBox.Show("Файл " + Save_dictionary.FileName + " сохранен", "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void OpenBinaryFormat(string file_name)
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            Save_dictionary open_dict = new Save_dictionary();
            using (Stream fStream = File.OpenRead(file_name))
            {
                open_dict = (Save_dictionary)binFormat.Deserialize(fStream);
            }
            /*Hosts = open_dict.Hosts;
            Servers = open_dict.Servers;
            Links = open_dict.Links;*/
            try
            {
                //Host
                foreach (KeyValuePair<int, host> pair_host in open_dict.Hosts)
                {
                    host new_host = new host(this);
                    new_host.id = pair_host.Value.id;
                    new_host.name = pair_host.Value.name;
                    new_host.delay_for_admission = pair_host.Value.delay_for_admission;
                    new_host.quque_for_transfer = pair_host.Value.quque_for_transfer;
                    new_host.a1 = pair_host.Value.a1;
                    new_host.a2 = pair_host.Value.a2;
                    new_host.func = pair_host.Value.func;
                    new_host.adress_to = pair_host.Value.adress_to;
                    Hosts.Add(new_host.id, new_host);
                    elements.Add(new_host.id, new_host);
                    new_host.secure_param.Key_Firewall = pair_host.Value.secure_param.Key_Firewall;
                    new_host.secure_param.Key_Antivirus = pair_host.Value.secure_param.Key_Antivirus;
                    new_host.secure_param.Key_IDS = pair_host.Value.secure_param.Key_IDS;
                    new_host.secure_param.Key_Administration = pair_host.Value.secure_param.Key_Administration;
                    new_host.secure_param.P_secure_start = pair_host.Value.secure_param.P_secure_start;
                    new_host.secure_param.P_virus_start = pair_host.Value.secure_param.P_virus_start;
                    new_host.secure_param.K_secure_set();
                    new_host.secure_param.P_apriory_set();
                    new_host.secure_param.P_return_secure_apriory_set();
                    new_host.label_name.Text = new_host.name;
                    if (open_dict.reports.Contains(new_host.id)) new_host.on_report_Click(null, new EventArgs());
                    MainContainer.Panel2.Controls.Add(new_host.panel);
                    MainContainer.Panel2.Controls.SetChildIndex(new_host.panel, 0);
                }
                //Server
                foreach (KeyValuePair<int, server> pair_serv in open_dict.Servers)
                {
                    server new_serv = new server(this);
                    new_serv.id = pair_serv.Value.id;
                    new_serv.name = pair_serv.Value.name;
                    new_serv.delay_for_admission = pair_serv.Value.delay_for_admission;
                    new_serv.quque_for_transfer = pair_serv.Value.quque_for_transfer;
                    new_serv.speed_do = pair_serv.Value.speed_do;
                    Servers.Add(new_serv.id, new_serv);
                    elements.Add(new_serv.id, new_serv);
                    new_serv.secure_param.Key_Firewall = pair_serv.Value.secure_param.Key_Firewall;
                    new_serv.secure_param.Key_Antivirus = pair_serv.Value.secure_param.Key_Antivirus;
                    new_serv.secure_param.Key_IDS = pair_serv.Value.secure_param.Key_IDS;
                    new_serv.secure_param.Key_Administration = pair_serv.Value.secure_param.Key_Administration;
                    new_serv.secure_param.P_secure_start = pair_serv.Value.secure_param.P_secure_start;
                    new_serv.secure_param.P_virus_start = pair_serv.Value.secure_param.P_virus_start;
                    new_serv.secure_param.K_secure_set();
                    new_serv.secure_param.P_apriory_set();
                    new_serv.secure_param.P_return_secure_apriory_set();
                    new_serv.label_name.Text = new_serv.name;
                    if (open_dict.reports.Contains(new_serv.id)) new_serv.on_report_Click(null, new EventArgs());
                    MainContainer.Panel2.Controls.Add(new_serv.panel);
                    MainContainer.Panel2.Controls.SetChildIndex(new_serv.panel, 0);
                }
                //Links
                foreach (KeyValuePair<int, link> pair_link in open_dict.Links)
                {
                    link new_link = new link(this);
                    new_link.adress_from = pair_link.Value.adress_from;
                    new_link.adress_to = pair_link.Value.adress_to;
                    new_link.speed = pair_link.Value.speed;
                    Links.Add(new_link.id, new_link);
                    new_link.point_from = pair_link.Value.point_from;
                    elements[new_link.adress_from].panel.Location = new_link.point_from;
                    new_link.point_to = pair_link.Value.point_to;
                    elements[new_link.adress_to].panel.Location = new_link.point_to;
                    new_link.line.StartPoint = new_link.point_from;
                    new_link.line.EndPoint = new_link.point_to;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Файл " + file_name + " имеет неверный формат или поврежден","Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Error);
                закрытьToolStripMenuItem_Click(null,new EventArgs());
            }
            /*reports_hosts = open_dict.reports_hosts;
            reports_servers = open_dict.reports_servers;*/
            Number_of_elem.Text = "Всего элементов:  " + elements.Count.ToString();
        }
        private bool test_bebore_run()
        {
            if (Hosts.Count == 0)
            {
                MessageBox.Show("В сети нет работающх генераторов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (Servers.Count == 0)
            {
                MessageBox.Show("В сети нет работающх серверов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            foreach (element k in elements.Values)
            {
                if (k.secure_param.n == 0)
                {
                    MessageBox.Show("Не все элементы соединены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }//если false, то проект нельзя запускать из-за ошибок
    }
    
    [Serializable]
    public class element
    {
        public element(Form1 tt)
        {
            t = tt;
            if (t.elements.Count == 0) id = 0;
            else
            {
                int i = 0;
                while (t.elements.ContainsKey(i))
                {
                    i++;
                }
                id = i;
            } 
            name = id.ToString();
            tick = 1000;
            label_name.Dock = DockStyle.Top;
            picture.Dock = DockStyle.Top;
            panel.Controls.Add(label_name);
            panel.Controls.Add(picture);
            picture.Enabled = false;
            label_name.Enabled = false;
            picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            picture.Name = id.ToString();
            label_name.Text = name;
            label_name.BackColor = Color.White;
            label_name.AutoSize = true;
            panel.Location = new System.Drawing.Point(100 + 10 * (t.elements.Count), 100 + 10 * (t.elements.Count));
            panel.Name = id.ToString();
            panel.BackColor = Color.White;
            panel.BorderStyle = BorderStyle.FixedSingle;
            panel.ContextMenuStrip = context_menu_element;
            // 
            // delete
            // 
            delete.Name = "deleteToolStripMenuItem";
            delete.Size = new System.Drawing.Size(152, 22);
            delete.Text = "Удалить";
            delete.Click += new EventHandler(delete_Click);
            // 
            // properties
            // 
            properties.Name = "propertiesToolStripMenuItem";
            properties.Size = new System.Drawing.Size(152, 22);
            properties.Text = "Свойства";
            properties.Click += new EventHandler(properties_Click);
            // 
            // link_to
            // 
            link_to.Name = "link_toToolStripMenuItem";
            link_to.Size = new System.Drawing.Size(152, 22);
            link_to.Text = "Связать";
            link_to.Click += new EventHandler(link_to_Click);
            // 
            // on_report
            // 
            on_report.Name = "on_reportToolStripMenuItem";
            on_report.Size = new System.Drawing.Size(152, 22);
            on_report.Text = "Добавить отчет";
            on_report.Click += new EventHandler(on_report_Click);
            // 
            // off_report
            // 
            off_report.Name = "off_reportToolStripMenuItem";
            off_report.Size = new System.Drawing.Size(152, 22);
            off_report.Text = "Удалить отчет";
            off_report.Visible = false;
            off_report.Click += new EventHandler(off_report_Click);
            // 
            // contextMenuStrip_element
            // 
            context_menu_element.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                link_to,
                on_report,
                off_report,
                delete,
                properties});
            context_menu_element.Name = "context_menu_element_"+id.ToString();
            context_menu_element.Size = new System.Drawing.Size(153, 70);
            panel.MouseDown += new MouseEventHandler(panel_MouseDown);
            panel.MouseUp += new MouseEventHandler(panel_MouseUp);
            panel.MouseMove += new MouseEventHandler(panel_MouseMove);
        }

        public void Dispose()
        {
            picture.Dispose();
            label_name.Dispose();
            panel.Dispose();
            context_menu_element.Dispose();
            properties.Dispose();
            delete.Dispose();
        }
        //События
        public virtual void delete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You enter delete!!!)))");
        }
        protected virtual void properties_Click(object sender, EventArgs e)//переопределяется в наследнике
        {
            MessageBox.Show("You enter properties!!!)))");
        }
        public virtual void on_report_Click(object sender, EventArgs e)
        {
            panel.BackColor = Color.LightBlue;
            label_name.BackColor = Color.LightBlue;
            on_report.Visible = false;
            off_report.Visible = true;
        }
        protected virtual void off_report_Click(object sender, EventArgs e)
        {
            panel.BackColor = Color.White;
            label_name.BackColor = Color.White;
            on_report.Visible = true;
            off_report.Visible = false;
        }
        void panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDown)
            {
                Panel c = sender as Panel;
                c.Location = (t.MainContainer.Panel2.PointToClient(Control.MousePosition));
                foreach(link l in t.Links.Values)
                {
                    if (this.id == l.adress_from)
                    {
                        l.line_redraw(c.Location,l.point_to);
                    }
                    if (this.id == l.adress_to)
                    {
                        l.line_redraw(l.point_from,c.Location);
                    }
                }
            }
        }
        void panel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            isDown = false;
            // MessageBox.Show(isDown.ToString());
        }
        void panel_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            isDown = true;
            //  MessageBox.Show(isDown.ToString());
        }
        void link_to_Click(object sender, EventArgs e)
        {
            properties_link prop_link = new properties_link(t, new link(t,id));
            prop_link.Show();
        }

        //переменные
        [NonSerialized]
        public PictureBox picture = new PictureBox();
        [NonSerialized]
        public Label label_name = new Label();
        [NonSerialized]
        public Panel panel = new Panel();
        [NonSerialized]
        protected bool isDown = false;//отслеживает нажата ли кнопка мыши для перемещения панели
        public int id;//идентификатор элемента
        [NonSerialized]
        public Form1 t;//ссылка на главную форму
        public string name;//имя блока
        public int tick;//тактовая частота
        public int delay_for_admission;//задержка на прием, сек
        public Dictionary<int, int> quque_for_transfer = new Dictionary<int,int>();//очередь на передачу, (адрес(id),бит)
        [NonSerialized]
        public static int timer = 0;//вспомогательная переменная для определения времени
        [NonSerialized]
        public static int sec = 1;
        [NonSerialized]
        public int data = 0;//вспомогательная переменная для определения общего количества сгенерированного/обработанного траффика
        public Secure secure_param = new Secure();
        [NonSerialized]
        private ContextMenuStrip context_menu_element = new ContextMenuStrip();
        [NonSerialized]
        private ToolStripMenuItem properties = new ToolStripMenuItem();
        [NonSerialized]
        private ToolStripMenuItem delete = new ToolStripMenuItem();
        [NonSerialized]
        private ToolStripMenuItem link_to = new ToolStripMenuItem();
        [NonSerialized]
        private ToolStripMenuItem on_report = new ToolStripMenuItem();
        [NonSerialized]
        private ToolStripMenuItem off_report = new ToolStripMenuItem();
    }

    [Serializable]
    public class host:element
    {
        public host(Form1 tt)
            : base(tt)
        {
            delay_for_admission = 0;
            picture.Image = global::NetModeler.Properties.Resources.host;
            picture.Size = new System.Drawing.Size(75, 52);
            label_name.Size = new Size(50, 10);
            panel.Size = new System.Drawing.Size(75, 68);
            adress_to = id;
        }
        //события
        protected override void properties_Click(object sender, EventArgs e)//переопределение события нажатия кнопки свойства
        {
            propertis_host prop_host = new propertis_host(t, this);
            prop_host.Show();
        }
        public override void delete_Click(object sender, EventArgs e)//переопределение события нажатия кнопки удалить
        {
            this.Dispose();
            t.Hosts.Remove(id);
            t.elements.Remove(id);
            if (t.reports.ContainsKey(id)) t.reports.Remove(id);
            if (t.reports_hosts.ContainsKey(id)) t.reports_hosts.Remove(id);
            t.Number_of_elem.Text = "Всего элементов:  " + t.elements.Count.ToString();
            List<int> del_list = new List<int>();//список id линий для удаления
            foreach (link l in t.Links.Values)
            {
                if ((this.id == l.adress_from)||(this.id == l.adress_to))
                {
                    del_list.Add(l.id);
                }
            }
            foreach (int i in del_list)
            {
                t.Links[i].Dispose();
                t.Links.Remove(i);
            }
        }
        public override void on_report_Click(object sender, EventArgs e)
        {
            base.on_report_Click(sender, e);
            Report_host r_host = new Report_host(t);
            r_host.id = id;
            r_host.name = name;
            t.reports.Add(id, r_host);
            t.reports_hosts.Add(id, r_host);
            //MessageBox.Show("Отчет добавлен");
        }//переопределение события нажатие на кнопку добавить отчет
        protected override void off_report_Click(object sender, EventArgs e)
        {
            base.off_report_Click(sender, e);
            if (t.reports.ContainsKey(id)) t.reports.Remove(id);
            if (t.reports_hosts.ContainsKey(id)) t.reports_hosts.Remove(id);
            //MessageBox.Show("Отчет удален");
        }//переопределение события нажатие на кнопку удалить отчет

        //переменные
        public int a1 = 0;//параметры для функции распределения
        public int a2 = 0;//
        public generation_of_func func = new generation_of_func(Functions.Beta);//функция генрации информации
        public int adress_to;
    }

    [Serializable]
    public class server:element
    {
        public server(Form1 tt)
            : base(tt)
        {
            queue = 0;
            delay_for_admission = 0;
            speed_do = 0;
            //
            picture.Image = global::NetModeler.Properties.Resources.serv2;
            picture.Size = new System.Drawing.Size(100, 100);
            label_name.Size = new Size(50, 10);
            panel.Size = new System.Drawing.Size(100, 116);
        }
        //события
        protected override void properties_Click(object sender, EventArgs e)//переопределение события нажатия кнопки свойства
        {
            properties_server prop_serv = new properties_server(t, this);
            prop_serv.Show();
        }
        public override void delete_Click(object sender, EventArgs e)
        {
            this.Dispose();
            t.Servers.Remove(id);
            t.elements.Remove(id);
            if (t.reports.ContainsKey(id)) t.reports.Remove(id);
            if (t.reports_servers.ContainsKey(id)) t.reports_servers.Remove(id);
            t.Number_of_elem.Text = "Всего элементов:  " + t.elements.Count.ToString();
            List<int> del_list = new List<int>();//список id линий для удаления
            foreach (link l in t.Links.Values)
            {
                if ((this.id == l.adress_from) || (this.id == l.adress_to))
                {
                    del_list.Add(l.id);
                }
            }
            foreach (int i in del_list)
            {
                t.Links[i].Dispose();
                t.Links.Remove(i);
            }
        }
        public override void on_report_Click(object sender, EventArgs e)
        {
            base.on_report_Click(sender, e);
            Report_server r_server = new Report_server(t);
            r_server.id = id;
            r_server.name = name;
            t.reports.Add(id, r_server);
            t.reports_servers.Add(id, r_server);
            //MessageBox.Show("Отчет добавлен");
        }//переопределение события нажатие на кнопку добавить отчет
        protected override void off_report_Click(object sender, EventArgs e)
        {
            base.off_report_Click(sender, e);
            if (t.reports.ContainsKey(id)) t.reports.Remove(id);
            if (t.reports_servers.ContainsKey(id)) t.reports_servers.Remove(id);
            //MessageBox.Show("Отчет удален");
        }//переопределение события нажатие на кнопку удалить отчет
        //переменные
        public int speed_do;//скорость обработки данных, бит/сек
        [NonSerialized]
        public int queue;//очередь на обработку, бит
    }

    public class ping : element
    {
        public ping(Form1 tt)
            : base(tt)
        {
            delay = 0;
        }
        //переменные
        public int delay;//время задержки
    }

    [Serializable]
    public class link
    {
        public link(Form1 tt)
        {
            t = tt;
            speed = 0;
            if (t.Links.Count == 0) id = 0;
            else
            {
                int i = 0;
                while (t.Links.ContainsKey(i))
                {
                    i++;
                }
                id = i;
            }
            adress_from = 0;
            adress_to = 0;
            cont_line.Parent = t.MainContainer.Panel2;
            line.Parent = cont_line;
            line.StartPoint = point_from;
           // line.Parent.Controls.SetChildIndex(line.GetContainerControl,0);
            line.StartPoint = point_to;
            line.ContextMenuStrip = context_menu_element;

            // 
            // delete
            // 
            delete.Name = "удалитьToolStripMenuItem";
            delete.Size = new System.Drawing.Size(152, 22);
            delete.Text = "Удалить";
            delete.Click += new EventHandler(delete_Click);
            // 
            // properties
            // 
            properties.Name = "свойстваToolStripMenuItem";
            properties.Size = new System.Drawing.Size(152, 22);
            properties.Text = "Свойства";
            properties.Click += new EventHandler(properties_Click);
            // 
            // contextMenuStrip_element
            // 
            context_menu_element.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            delete,
            properties});
            context_menu_element.Name = "context_menu_element_" + id.ToString();
            context_menu_element.Size = new System.Drawing.Size(153, 70);
        }
        public link(Form1 tt, int from_adress):this(tt)
        {
            adress_from = from_adress;
        }
        public void line_redraw(Point from, Point to)
        {
            point_from = from;
            point_to = to;
            line.StartPoint = point_from;
            line.EndPoint = point_to;
        }//перерисовка линии
        public void Dispose()
        {
            context_menu_element.Dispose();
            properties.Dispose();
            delete.Dispose();
            cont_line.Dispose();
            line.Dispose();
        }
        //события
        void properties_Click(object sender, EventArgs e)
        {
            properties_link prop_link = new properties_link(t, this);
            prop_link.Show();
        }
        void delete_Click(object sender, EventArgs e)
        {
            this.Dispose();
            t.Links.Remove(id);
        }
        //переменные
        public int speed;//скорость передачи
        public int id;
        public int adress_from;//адресс отправителя
        public int adress_to;//адресс получателя
        [NonSerialized]
        public Form1 t;//ссылка на главную форму
        [NonSerialized]
        private ContextMenuStrip context_menu_element = new ContextMenuStrip();//контекстное мнею для линий связи
        [NonSerialized]
        private ToolStripMenuItem properties = new ToolStripMenuItem();
        [NonSerialized]
        private ToolStripMenuItem delete = new ToolStripMenuItem();
        [NonSerialized]
        public ShapeContainer cont_line = new ShapeContainer();//контейнер для рисования линии
        [NonSerialized]
        public LineShape line = new LineShape();//линия
        //[NonSerialized]
        public Point point_from = new Point();//координаты точки начала
        //[NonSerialized]
        public Point point_to = new Point();//координаты точки конца
    }

    [Serializable]
    public class Save_dictionary
    {
        public Save_dictionary(Form1 t)
        {
            Hosts = t.Hosts;
            Servers = t.Servers;
            Links = t.Links;
            foreach (int id in t.reports.Keys)
            {
                reports.Add(id);
            }
        }
        public Save_dictionary()
        {

        }
        public Dictionary<int, host> Hosts = new Dictionary<int, host>();
        public Dictionary<int, server> Servers = new Dictionary<int, server>();
        public Dictionary<int, link> Links = new Dictionary<int, link>();
        public List<int> reports = new List<int>();
        public static string FileName;
    }
}
