﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetModeler
{
    [Serializable]
    public class Secure
    {
        public Secure()
        {
            Key_virus = false;
            K_secure = 0;
            P_virus_start = 0.35;
            P_secure_start = 0;
            P_return_secure_apriory = 0.1;
            P_apriory_set();
        }
        //функции
        public void P_apriory_set()
        {
            P_apriory = P_virus_start * (1 - K_secure);
        }//установка априорной вероятности заражения
        public void P_return_secure_apriory_set()
        {
            double sum = 0;
            if (Key_Antivirus)
            {
                if (Key_Administration) sum = 0.4;
                else sum = 0.3;
            }
            else
            {
                if (Key_Administration) sum = 0.2;
                else sum = 0.1;
            }
            P_return_secure_apriory = sum + P_secure_start;
        }//установка априорной вероятности восстановления работы
        public void P_return_secure_dinamic_set(int t)
        {
            P_return_secure_dinamic = P_return_secure_apriory+0.01*t;
        }//установка динамической вероятности заражения, m - количество зараженных соседей
        public void P_dinamic_set(int m)
        {
            P_dinamic = ((0.9*m)/n + 1)*P_apriory;
        }//установка динамической вероятности восстановления работы, t - время заражения
        public void K_secure_set()
        {
            Dictionary<double, bool> q = new Dictionary<double, bool>();
            double sum = 0;
            q.Add(0.25, Key_Firewall);
            q.Add(0.15, Key_Antivirus);
            q.Add(0.3, Key_IDS);
            q.Add(0.1, Key_Administration);
            foreach (KeyValuePair<double, bool> s in q)
            {
                if (s.Value) sum += s.Key;
            }
            K_secure = sum;
        }//установка коэффициента защиты
        public void Clear()
        {
            Key_virus=false;
            n=0;//флаг заражения
            T_virus_all=0;//общее время заражения
        }
        //переменные
        public bool Is_server = false;
        public bool Key_virus;//флаг заражения
        public int n;//количество соседей
        public bool Key_Firewall = false;//флаг
        public bool Key_Antivirus = false;//флаг 
        public bool Key_IDS = false;//флаг 
        public bool Key_Administration = false;//флаг 
        public int t_virus_start;//начальное время заражения
        public int T_virus_all=0;//общее время заражения
        public double P_return_secure_apriory;//априорная вероятность восстановления работы
        public double P_return_secure_dinamic;//динамическая вероятность восстановления работы
        public double P_virus_start;//начальная вероятность заражения
        public double P_secure_start;//начальная веротность восстановления
        public double K_secure;//коэффициент защиты
        public double P_apriory;//априорная вероятность заражения
        public double P_dinamic;//динамическая вероятность заражения
    }
}
