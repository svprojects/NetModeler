﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace NetModeler
{
    public class Run
    {
        public Run(Run_properties rr,Form1 tt)
        {
            t = tt;
            r = rr;
        }
        private static int Deykstra(Dictionary<int, Dictionary<int, int>> z, int start, int finish)
        {
            int n = z.Count;
            int v;
            int p = n;
            int infinity = int.MaxValue;//Бесконечность
            int way = start; ;
            int s = start;//Номер исходной вершины 
            int g = finish;//Номер конечной вершины
            Dictionary<int, int> x = new Dictionary<int, int>();//Массив, содержащий единицы и нули для каждой вершины,
            // x[i]=0 - еще не найден кратчайший путь в i-ю вершину,
            // x[i]=1 - кратчайший путь в i-ю вершину уже найден
            Dictionary<int, int> t = new Dictionary<int, int>();//t[i] - длина кратчайшего пути от вершины s в i
            Dictionary<int, int> h = new Dictionary<int, int>();//h[i] - вершина, предшествующая i-й вершине на кратчайшем пути
            //Инициализируем начальные значения массивов
            foreach (int e in z.Keys)
            {
                t.Add(e, infinity);
                x.Add(e, 0);
                h.Add(e, 0);
            }
            int u = 0;//Счетчик вершин
            h[s] = 0;//s - начало пути, поэтому этой вершине ничего не предшествует
            t[s] = 0;//Кратчайший путь из s в s равен 0
            x[s] = 1;//Для вершины s найден кратчайший путь
            v = s;//Делаем s текущей вершиной
            while (true)
            {
                // Перебираем все вершины, смежные v, и ищем для них кратчайший путь
                foreach (int key in x.Keys)
                {
                    if (z[v][key] == 0) continue; // Вершины u и v несмежные
                    if (x[key] == 0 && t[key] > t[v] + z[v][key]) //Если для вершины key еще не
                    //найден кратчайший путь и новый путь в u короче чем старый, то
                    {
                        t[key] = t[v] + z[v][key];//запоминаем более короткую длину пути в массив t и
                        h[key] = v;//запоминаем, что v->u часть кратчайшего пути из s->u
                    }
                }
                // Ищем из всех длин некратчайших путей самый короткий
                int w = infinity;//Для поиска самого короткого пути
                v = -1;//В конце поиска v - вершина, в которую будет найден новый кратчайший путь. 
                //Она станет текущей вершиной
                foreach (int key in x.Keys) // Перебираем все вершины.
                {
                    if (x[key] == 0 && t[key] < w) // Если для вершины не найден кратчайший
                    // путь и если длина пути в вершину key меньше уже найденной, то
                    {
                        v = key; // текущей вершиной становится u-я вершина
                        w = t[key];
                    }
                }
                if (v == -1)
                {
                    MessageBox.Show("No way from top " + (s + 1) + " to top  " + (g + 1) + "\n");
                    break;
                }
                if (v == g)//Найден кратчайший путь,
                {
                    u = g;
                    while (u != s)
                    {
                        way = u;
                        u = h[u];
                    }
                    break;
                }
                x[v] = 1;
            }
            return way;
        }//Алгоритм Дейкстры.
        //Поиск кратчайшего пути из одной врешины в другую. 
        //Возвращает значение адреса следующей по пути вершины
        private static int Num_of_virus(int id)
        {
            int num = 0;
            foreach (KeyValuePair<int,int> item in Form1.table_of_way[id])
            {
                if (item.Value != 0)//если элемент соединен
                {
                    if (t.elements[item.Key].secure_param.Key_virus)
                        num++;
                }
            }
            return num;
        }//возвращает количество зараженных соседей
        public static void Run_server(object S)
        {
            RichTextBox rtb1 = r.richTextBox1;
            server Serv = S as server;
            int server_do_save = 0;
            while (element.timer > 0)
            {
                //выполнение работы по обработки поступившего траффика
                lock(queque_lock)
                {
                    if ((Serv.queue - Serv.speed_do) >= 0)
                    {
                        Serv.queue -= Serv.speed_do;
                        Serv.data += Serv.speed_do;
                    }
                    else
                    {
                        Serv.data += Serv.queue;
                        Serv.queue = 0;
                    }
                    if (Serv.queue < 0) Serv.queue = 0;
                    Thread.Sleep(1000 / element.sec); //Thread.Sleep(1000 * element.sec);
                }
                if (Serv.secure_param.Key_virus)//если заражен
                {
                    int t_virus = 0;
                    t_virus = Serv.secure_param.t_virus_start - element.timer * element.sec / 1000;//вычисляем прошедшее с начала заражения время
                    Serv.secure_param.P_return_secure_dinamic_set(t_virus);//генерируем вероятность восстановления работы
                    if (Functions.Random_zero_one(Serv.secure_param.P_return_secure_dinamic))//если происходи восстановление
                    {
                        Serv.secure_param.Key_virus = false;
                        Serv.secure_param.T_virus_all += t_virus;
                        Serv.speed_do = server_do_save;//сервер восстанавливает свою работу с прежней скоростью
                        rtb1.Invoke(new MethodInvoker(() =>
                        {
                            r.richTextBox1.Text += " Сервер " + Serv.name + "восстановлен " + "\n";
                        }));
                    }
                }
                else//не заражен
                {
                    Serv.secure_param.P_dinamic_set(Num_of_virus(Serv.id));//генерируем динамическую вероятность заражения
                    rtb1.Invoke(new MethodInvoker(() =>
                    {
                        r.richTextBox1.Text += "вероятность заражения " + Serv.secure_param.P_dinamic.ToString() + " !!!!!! " + "\n";
                    }));
                    if (Functions.Random_zero_one(Serv.secure_param.P_dinamic))//если происходи заражение
                    {
                        Serv.secure_param.Key_virus = true;//устанавливаем флаг заражения
                        Serv.secure_param.t_virus_start = element.timer * element.sec / 1000;//устанавливаем время заражения
                        server_do_save = Serv.speed_do;//сохраняем скорость работы сервера
                        Serv.speed_do = 0;//сервер перестает исполнять полезную работу
                        rtb1.Invoke(new MethodInvoker(() =>
                        {
                            r.richTextBox1.Text += "Сервер " + Serv.name + " заражен " + "\n";
                        }));
                    }
                }
            }
            //MessageBox.Show("Run_server, работа завершена!\n Serv.data = " + Serv.data.ToString() + " \n Serv.queue = " + Serv.queue.ToString());
        }//работа сервера по обработке траффика
        public static void Run_host(object H)
        {
            RichTextBox rtb1 = r.richTextBox1;
            host host_this = H as host;
            int arg1 = host_this.a1;
            int arg2 = host_this.a2;
            generation_of_func save_func = new generation_of_func(Functions.Standart);
            while (element.timer > 0)
            {
                //выполнение работы по генерации траффика поступившего траффика
                int gen_bit = 0;//сгенерированное количество бит по заданному закону распределения
                gen_bit = host_this.func(host_this.a1, host_this.a2);
                if (gen_bit < 0)
                { gen_bit = (-1) * gen_bit; }
                //отправляем траффик в очередь на передачу
                lock (queque_for_transfer_lock)
                {
                    if (host_this.quque_for_transfer.ContainsKey(host_this.adress_to))
                    {
                        host_this.quque_for_transfer[host_this.adress_to] += gen_bit;
                        //MessageBox.MessageBox.Show("Run_host, сгенерировал траффик!");
                    }
                    else
                        host_this.quque_for_transfer.Add(host_this.adress_to, gen_bit);
                        //MessageBox.Show("Run_host, добавил с новым адрессом!");
                }
                Thread.Sleep(1000 / element.sec);
                if (host_this.secure_param.Key_virus)//если заражен
                {
                    int t_virus = 0;
                    t_virus = host_this.secure_param.t_virus_start - element.timer * element.sec / 1000;//вычисляем прошедшее с начала заражения время
                    host_this.secure_param.P_return_secure_dinamic_set(t_virus);//генерируем вероятность восстановления работы
                    if (Functions.Random_zero_one(host_this.secure_param.P_return_secure_dinamic))//если происходи восстановление
                    {
                        host_this.secure_param.Key_virus = false;
                        host_this.secure_param.T_virus_all += t_virus;
                        host_this.func = save_func;
                        host_this.a1 = arg1;//сохраняем аргументы функции
                        host_this.a2 = arg2;
                        rtb1.Invoke(new MethodInvoker(() =>
                        {
                            r.richTextBox1.Text += " Хост " + host_this.name + " восстановлен " + "\n";
                        }));
                    }
                }
                else//не заражен
                {
                    host_this.secure_param.P_dinamic_set(Num_of_virus(host_this.id));//генерируем динамическую вероятность заражения
                    if (Functions.Random_zero_one(host_this.secure_param.P_dinamic))//если происходи заражение
                    {
                        host_this.secure_param.Key_virus = true;//устанавливаем флаг заражения
                        host_this.secure_param.t_virus_start = element.timer * element.sec / 1000;//устанавливаем время заражения
                        save_func = host_this.func;//сохраняем функцию генерации пакетов
                        arg1 = host_this.a1;//сохраняем аргументы функции
                        arg2 = host_this.a2;
                        host_this.func = Functions.Standart;//меняем функцию на стандартную
                        host_this.a1 = 1000;//меняем аргументы функции для посылки максимально большого количества информации
                        host_this.a2 = 0;
                        rtb1.Invoke(new MethodInvoker(() =>
                        {
                            r.richTextBox1.Text += " Хост " + host_this.name + " заражен " + "\n";
                        }));
                    }
                }
            }
            host_this.a1 = arg1;//восстанавливаем аргументы функции
            host_this.a2 = arg2;
            //MessageBox.Show("Run_host, работа завершена!");
        }//работа хоста по генерации траффика
        public static void Run_transfer(object E)
        {
            element Elem = E as element;
            int x_Key;
            RichTextBox rtb1 = r.richTextBox1;
            while (element.timer > 0)
            {
                if (Elem.quque_for_transfer.Count > 0)
                {
                    //MessageBox.Show("Elem.quque_for_transfer.Count =  " + Elem.id+" пробуем запустить перебор");
                    //MessageBox.Show("Проверка на соединение: " + Elem.id + " с 0 = " + Form1.table_of_way[Elem.id][0]);
                    //передача траффика из очереди на передачу
                    lock (queque_for_transfer_lock)
                    {
                        lock (queque_lock)
                        {
                            x_Key = Elem.quque_for_transfer.First().Key;
                            //если соединен
                            rtb1.Invoke(new MethodInvoker(() =>
                            {
                                r.richTextBox1.Text += "Проверка на соединение: " + Elem.id + " с " + x_Key + " = " + Form1.table_of_way[Elem.id][x_Key] + "\n";
                            }));
                            if (Form1.table_of_way[Elem.id][x_Key] != 0)
                            {
                                //это сервер??
                                rtb1.Invoke(new MethodInvoker(() =>
                                {
                                    r.richTextBox1.Text += "Соединен " + Elem.id+"\n";
                                }));
                                if (t.Servers.ContainsKey(x_Key))
                                {
                                    rtb1.Invoke(new MethodInvoker(() =>
                                    {
                                        r.richTextBox1.Text += "Передача в очередь в " + x_Key + "\n"; 
                                    }));
                                    //передаем траффик в очередь сервера
                                    rtb1.Invoke(new MethodInvoker(() =>
                                    { 
                                        r.richTextBox1.Text += "В закрытой области... \n"; 
                                    }));
                                    if ((Elem.quque_for_transfer[x_Key] - Form1.table_of_way[Elem.id][x_Key]) >= 0)
                                    {
                                        t.Servers[x_Key].queue += Form1.table_of_way[Elem.id][x_Key];
                                        //снимаем траффик с очереди на передачу
                                        Elem.quque_for_transfer[x_Key] -= Form1.table_of_way[Elem.id][x_Key];
                                    }
                                    else
                                    {
                                        t.Servers[x_Key].queue += Elem.quque_for_transfer[x_Key];
                                        //снимаем траффик с очереди на передачу
                                        Elem.quque_for_transfer[x_Key] = 0;
                                    }
                                    rtb1.Invoke(new MethodInvoker(() =>
                                    { 
                                        r.richTextBox1.Text += "Изменили коллекцию. Теперь сон \n"; 
                                    }));
                                    Thread.Sleep(1000 * (1 + t.Servers[x_Key].delay_for_admission) / element.sec); //Thread.Sleep(element.sec * 1000 * (1 + t.Servers[x_Key].delay_for_admission));
                                    rtb1.Invoke(new MethodInvoker(() =>
                                    { 
                                        r.richTextBox1.Text += "Хватит спать \n"; 
                                    }));
                                    //удаление элемента
                                    if (Elem.quque_for_transfer[x_Key] <= 0) Elem.quque_for_transfer.Remove(x_Key);
                                }
                                //это не сервер...
                                else
                                {
                                    MessageBox.Show("Error! It isn't server!");
                                    Elem.quque_for_transfer.Remove(x_Key);
                                }
                            }
                            //если не соединен напрямую
                            else
                            {
                                rtb1.Invoke(new MethodInvoker(() =>
                                { 
                                    r.richTextBox1.Text += "Не соединен \n"; 
                                }));
                                //определяем следующий адрес по ближайшему маршруту
                                int next_point_of_way = Deykstra(Form1.table_of_way, Elem.id, x_Key);
                                //создаем на следующм элементе объект для очереди на передачу
                                if (!t.elements[next_point_of_way].quque_for_transfer.ContainsKey(x_Key))
                                {
                                    t.elements[next_point_of_way].quque_for_transfer.Add(x_Key, 0);
                                }
                                //передаем траффик
                                if ((Elem.quque_for_transfer[x_Key] - Form1.table_of_way[Elem.id][next_point_of_way]) >= 0)
                                {
                                    t.elements[next_point_of_way].quque_for_transfer[x_Key] += Form1.table_of_way[Elem.id][next_point_of_way];
                                    //снимаем траффик с очереди на передачу
                                    Elem.quque_for_transfer[x_Key] -= Form1.table_of_way[Elem.id][next_point_of_way];
                                }
                                else
                                {
                                    t.elements[next_point_of_way].quque_for_transfer[x_Key] += Elem.quque_for_transfer[x_Key];
                                    //снимаем траффик с очереди на передачу
                                    Elem.quque_for_transfer[x_Key] = 0;
                                }
                                Thread.Sleep(1000 * (1 + t.Servers[x_Key].delay_for_admission) / element.sec); //Thread.Sleep(element.sec * 1000 * (1 + t.Servers[x_Key].delay_for_admission));
                                if (Elem.quque_for_transfer[x_Key] <= 0) Elem.quque_for_transfer.Remove(x_Key);
                            }
                            rtb1.Invoke(new MethodInvoker(() =>
                            { 
                                r.richTextBox1.Text += "Закончили \n"; 
                            }));
                        }
                    }
                } 
            }
        }//работа всех элементов по перемещению траффика в сети
        //переменные
        private static Run_properties r;
        private static Form1 t;
        private static object queque_lock = new object();//объект блокировки для переменной queque в классе server
        private static object queque_for_transfer_lock = new object();//объект блокировки для 
        //переменной queque_for_transfer в классе element
    }
}
