﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Troschuetz.Random;

namespace NetModeler
{
    [Serializable]
    class Functions
    {
        static Functions()
        {
            all_func.Add("Стандарт");
            all_func.Add("Бета");
            all_func.Add("Бета-премьер");
            all_func.Add("Коши");
            all_func.Add("Хи-кадрат");
            all_func.Add("Равномерное");
            all_func.Add("Эрланга");
            all_func.Add("Экспоненциальное");
            all_func.Add("Фишера-Снедекора");
            all_func.Add("Гамма");
            all_func.Add("Лапласа");
            all_func.Add("Логонормальное");
            all_func.Add("Нормальное");
            all_func.Add("Парето");
            all_func.Add("Рэлея");
            all_func.Add("Стьюдента");
            all_func.Add("Вейбулла");
        }
        public static int Standart(int bit, int a0)
        {
            return bit;
        }//стандартная фукнция, возвращающая заданное количество бит без изменений
        public static int Beta(int a, int b)
        {
            rand_Beta.Alpha = a;
            rand_Beta.Beta = b;
            return (int)rand_Beta.NextDouble();
        }//бета-распределение
        public static int Beta_prime(int a, int b)
        {
            rand_Beta_prime.Alpha = a;
            rand_Beta_prime.Beta = b;
            return (int)rand_Beta_prime.NextDouble();
        }//бета-премьер
        public static int Cauchy(int a, int y)
        {
            rand_Cauchy.Alpha = a;
            rand_Cauchy.Gamma = y;
            return (int)rand_Cauchy.NextDouble();
        }//распределение Коши
        public static int ChiSquare(int a, int b0)
        {
            rand_ChiSquare.Alpha = a;
            return (int)rand_ChiSquare.NextDouble();
        }//Хи-кадрат
        public static int ContinuousUniform(int a, int b)
        {
            rand_ContinuousUniform.Alpha = a;
            rand_ContinuousUniform.Beta = b;
            return (int)rand_ContinuousUniform.NextDouble();
        }//равномерное
        public static int Erlang(int a, int y)
        {
            rand_Erlang.Alpha = a;
            rand_Erlang.Lambda = y;
            return (int)rand_Erlang.NextDouble();
        }//Эрланг   
        public static int Exponential(int g, int b0)
        {
            rand_Exponential.Lambda = g;
            return (int)rand_Exponential.NextDouble();
        }//Экспоненциальное
        public static int FisherSnedecor(int a, int b)
        {
            rand_FisherSnedecor.Alpha = a;
            rand_FisherSnedecor.Beta = b;
            return (int)rand_FisherSnedecor.NextDouble();
        }//Фишера-Снедекора
        public static int Gamma(int a, int b)
        {
            rand_Gamma.Alpha = a;
            rand_Gamma.Theta = b;
            return (int)rand_Gamma.NextDouble();
        }//Гамма
        public static int Laplace(int a, int b)
        {
            rand_Laplace.Alpha = a;
            rand_Laplace.Mu = b;
            return (int)rand_Laplace.NextDouble();
        }//Лапласа
        public static int Lognormal(int a, int b)
        {
            rand_Lognormal.Mu = a;
            rand_Lognormal.Sigma = b;
            return (int)rand_Lognormal.NextDouble();
        }//Логонормальное
        public static int Normal(int a, int b)
        {
            rand_Normal.Mu = a;
            rand_Normal.Sigma = b;
            return (int)rand_Normal.NextDouble();
        }//Нормальное
        public static int Pareto(int a, int b)
        {
            rand_Pareto.Alpha = a;
            rand_Pareto.Beta = b;
            return (int)rand_Pareto.NextDouble();
        }//Парето
        public static int Rayleigh(int a, int b0)
        {
            rand_Rayleigh.Sigma = a;
            return (int)rand_Rayleigh.NextDouble();
        }//Рэлея
        public static int StudentsT(int a, int b0)
        {
            rand_StudentsT.Nu = a;
            return (int)rand_StudentsT.NextDouble();
        }//Стьюдента
        public static int Weibull(int a, int b)
        {
            rand_Weibull.Alpha = a;
            rand_Weibull.Lambda = b;
            return (int)rand_Weibull.NextDouble();
        }//Вейбулла
        public static bool Random_zero_one(double p)
        {
            double r = 0;
            if (p == 1) return true;
            if (p == 0) return false;
            r = rand.NextDouble() + p;
            if (r > 1) return true;
            else return false;
        }//генерация булевого значения true с определенной вероятностью
        //переменные
        public static List<string> all_func = new List<string>();
        private static Random rand = new Random();
        private static BetaDistribution rand_Beta = new BetaDistribution();
        private static BetaPrimeDistribution rand_Beta_prime = new BetaPrimeDistribution();
        private static CauchyDistribution rand_Cauchy = new CauchyDistribution();
        private static ChiSquareDistribution rand_ChiSquare = new ChiSquareDistribution();
        private static ContinuousUniformDistribution rand_ContinuousUniform = new ContinuousUniformDistribution();
        private static ErlangDistribution rand_Erlang = new ErlangDistribution();
        private static ExponentialDistribution rand_Exponential = new ExponentialDistribution();
        private static FisherSnedecorDistribution rand_FisherSnedecor = new FisherSnedecorDistribution();
        private static GammaDistribution rand_Gamma = new GammaDistribution();
        private static LaplaceDistribution rand_Laplace = new LaplaceDistribution();
        private static LognormalDistribution rand_Lognormal = new LognormalDistribution();
        private static NormalDistribution rand_Normal = new NormalDistribution();
        private static ParetoDistribution rand_Pareto = new ParetoDistribution();
        private static RayleighDistribution rand_Rayleigh = new RayleighDistribution();
        private static StudentsTDistribution rand_StudentsT = new StudentsTDistribution();
        private static WeibullDistribution rand_Weibull = new WeibullDistribution();
    }
}
