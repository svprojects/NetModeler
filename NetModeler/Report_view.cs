﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetModeler
{
    public partial class Report_view : Form
    {
        public Report_view(Form1 tt)
        {
            t = tt;
            InitializeComponent();
            Initialization();
        }
        public void Initialization()
        {
            groupBox_host.Visible = false;
            groupBox_server.Visible = false;
            groupBox_host.Dock = DockStyle.Top;
            groupBox_server.Dock = DockStyle.Top;
            groupBox_host.AutoSize = true;
            foreach (KeyValuePair<int, Report_server> rep in t.reports_servers)
            {
                Label lab_s = new Label();
                lab_s.Text = rep.Value.name;
                lab_s.Name = rep.Key.ToString();
                lab_s.Font = new Font("Times New Roman", 9 + 75 / 100);
                lab_s.Dock = DockStyle.Top;
                lab_s.AutoSize = false;
                lab_s.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
                lab_s.Margin = new System.Windows.Forms.Padding(0);
                lab_s.Height = 14;
                lab_s.MouseMove += new MouseEventHandler(lab_MouseMove);
                lab_s.MouseLeave += new EventHandler(lab_MouseLeave);
                lab_s.Click += new EventHandler(lab_s_Click);
                splitContainer_main.Panel1.Controls.Add(lab_s);
            }
            splitContainer_main.Panel1.Padding = new System.Windows.Forms.Padding(1);
            Label lab_server = new Label();
            lab_server.Text = "Серверы: ";
            lab_server.Font = new Font("Times New Roman", 12);
            lab_server.Dock = DockStyle.Top;
            splitContainer_main.Panel1.Controls.Add(lab_server);
            foreach (KeyValuePair<int, Report_host> rep in t.reports_hosts)
            {
                Label lab_h = new Label();
                lab_h.Text = rep.Value.name;
                lab_h.Name = rep.Key.ToString();
                lab_h.Font = new Font("Times New Roman", 9+75/100);
                lab_h.Dock = DockStyle.Top;
                lab_h.AutoSize = false;
                lab_h.Padding = new System.Windows.Forms.Padding(5,0,0,0);
                lab_h.Margin = new System.Windows.Forms.Padding(0);
                lab_h.Height = 14;
                lab_h.MouseMove += new MouseEventHandler(lab_MouseMove);
                lab_h.MouseLeave += new EventHandler(lab_MouseLeave);
                lab_h.Click += new EventHandler(lab_h_Click);
                splitContainer_main.Panel1.Controls.Add(lab_h);
            }
            splitContainer_main.Panel1.Padding = new System.Windows.Forms.Padding(1);
            Label lab_host = new Label();
            lab_host.Text = "Генераторы: ";
            lab_host.Font = new Font("Times New Roman", 12);
            lab_host.Dock = DockStyle.Top;
            splitContainer_main.Panel1.Controls.Add(lab_host);
        }
        //события
        void lab_h_Click(object sender, EventArgs e)
        {
            groupBox_server.Visible = false;
            groupBox_host.Visible = true;
            Label l = sender as Label;
            int id = Convert.ToInt32(l.Name);
            groupBox_host_Init(id);
        }
        void lab_s_Click(object sender, EventArgs e)
        {
            groupBox_host.Visible = false;
            groupBox_server.Visible = true;
            Label l = sender as Label;
            int id = Convert.ToInt32(l.Name);
            groupBox_server_Init(id);
        }
        void lab_MouseLeave(object sender, EventArgs e)
        {
            Label l = sender as Label;
            l.Font = new Font("Times New Roman", 9 + 75 / 100, FontStyle.Regular);
            l.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            l.ForeColor = Color.Black;
        }
        void lab_MouseMove(object sender, MouseEventArgs e)
        {
            Label l = sender as Label;
            l.Font = new Font("Times New Roman", 10, FontStyle.Underline);
            l.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            l.ForeColor = Color.LightBlue;
        }
        //функции
        private void groupBox_host_Init(int id)
        {
            groupBox_host.Text = t.Hosts[id].name;
            label_id_host.Text = "id: "+id.ToString();
            label_dfa_host.Text = "Задержка на прием: "+t.Hosts[id].delay_for_admission.ToString();
            label_func_host.Text = "Функция генерации траффика: " + propertis_host.Init_func(t.Hosts[id].func.Method.Name);
            label_adress_to_host.Text = "Адресс сервера обработки траффика: "+t.Hosts[id].adress_to.ToString();
            label_step_h.Text = "Степень вершины: " + t.Hosts[id].secure_param.n.ToString();
            label_time_virus_all_h.Text = "Общее время простоя: " + t.Hosts[id].secure_param.T_virus_all.ToString();
            label_P_secure_start_host.Text = "Начальная вероятность восстановления: " + t.Hosts[id].secure_param.P_secure_start.ToString();
            label_P_virus_start_host.Text = "Начальная вероятность заражения: " + t.Hosts[id].secure_param.P_virus_start.ToString();
            label_aprs_h.Text = "Априорная вероятность восстановления работы: " + t.Hosts[id].secure_param.P_return_secure_apriory.ToString();
            label_k_secure_h.Text = "Коэффициент защиты: " + t.Hosts[id].secure_param.K_secure.ToString();
            label_p_virus_h.Text = "Априорная вероятность заражения: " + t.Hosts[id].secure_param.P_apriory.ToString();

            List<int> max_list = new List<int>();
            double value = (t.reports_hosts[id].quque_for_transfer_time.Values).Max();
            foreach (double val in t.reports_hosts[id].virus.Values)
            {
                if(val!=0)
                max_list.Add(Convert.ToInt32(value));
                else max_list.Add(Convert.ToInt32(0));
            }
            //график очереди на передачу
            chart_host.Series[0].Points.DataBindXY(t.reports_hosts[id].quque_for_transfer_time.Keys, "время",
                t.reports_hosts[id].quque_for_transfer_time.Values, "количество");
            chart_host.Series["virus"].Points.DataBindXY(t.reports_hosts[id].virus.Keys, "время",
                    max_list, "количество");
            //график вероятностей заражения и восстановления
            chart_virus_h.Series["Заражение"].Points.DataBindXY(t.reports_hosts[id].p_virus_time.Keys, "время",
                    t.reports_hosts[id].p_virus_time.Values, "количество");
            chart_virus_h.Series["Восстановление"].Points.DataBindXY(t.reports_hosts[id].p_return_secure_time.Keys, "время",
                    t.reports_hosts[id].p_return_secure_time.Values, "количество");
            chart_virus_h.Series["virus"].Points.DataBindXY(t.reports_hosts[id].virus.Keys, "время",
                    t.reports_hosts[id].virus.Values, "количество");

        }
        private void groupBox_server_Init(int id)
        {
            groupBox_server.Text = t.Servers[id].name;
            label_id_serv.Text = "id: " + id.ToString();
            label_speed_do.Text = "Скорость обработки данных: " + t.Servers[id].speed_do.ToString();
            label_do_all.Text = "Количество обработанных данных: " + t.reports_servers[id].do_all.ToString();
            label_speed_middle.Text = "Средняя скорость обработки данных: " + t.reports_servers[id].speed_middle.ToString();
            label_step.Text = "Степень вершины: " + t.Servers[id].secure_param.n.ToString();
            label_time_virus_all.Text = "Общее время простоя: " + t.Servers[id].secure_param.T_virus_all.ToString();
            label_P_secure_start_serv.Text = "Начальная вероятность восстановления: " + t.Servers[id].secure_param.P_secure_start.ToString();
            label_P_virus_start_serv.Text = "Начальная вероятность заражения: " + t.Servers[id].secure_param.P_virus_start.ToString();
            label_aprs.Text = "Априорная вероятность восстановления работы: " + t.Servers[id].secure_param.P_return_secure_apriory.ToString();
            label_k_secure.Text = "Коэффициент защиты: " + t.Servers[id].secure_param.K_secure.ToString();
            label_p_virus.Text = "Априорная вероятность заражения: " + t.Servers[id].secure_param.P_apriory.ToString();
            List<int> max_list = new List<int>();
            double value = (t.reports_servers[id].quque_time.Values).Max();
            foreach (double val in t.reports_servers[id].virus.Values)
            {
                if (val != 0)
                    max_list.Add(Convert.ToInt32(value));
                else max_list.Add(Convert.ToInt32(0));
            }
            //график очереди на обработку
            chart_serv_queque.Series[0].Points.DataBindXY(t.reports_servers[id].quque_time.Keys, "время",
                    t.reports_servers[id].quque_time.Values, "количество");
            chart_serv_queque.Series["virus"].Points.DataBindXY(t.reports_servers[id].virus.Keys, "время",
                    max_list, "количество");
            //график очереди на передачу
            chart_serv_qfa.Series[0].Points.DataBindXY(t.reports_servers[id].quque_for_transfer_time.Keys, "время",
                    t.reports_servers[id].quque_for_transfer_time.Values, "количество");
            //график вероятностей заражения и восстановления
            chart_p.Series["Заражение"].Points.DataBindXY(t.reports_servers[id].p_virus_time.Keys, "время",
                    t.reports_servers[id].p_virus_time.Values, "количество");
            chart_p.Series["Восстановление"].Points.DataBindXY(t.reports_servers[id].p_return_secure_time.Keys, "время",
                    t.reports_servers[id].p_return_secure_time.Values, "количество");
            max_list.Clear();
            chart_p.Series["virus"].Points.DataBindXY(t.reports_servers[id].virus.Keys, "время",
                    t.reports_servers[id].virus.Values, "количество");
        }
        //переменные
        Form1 t;
    }
}
